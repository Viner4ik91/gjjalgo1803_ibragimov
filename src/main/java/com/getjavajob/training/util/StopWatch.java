package com.getjavajob.training.util;

import static java.lang.System.currentTimeMillis;

/**
 * @author vinerI
 */
public class StopWatch {
    private long start;

    public long start() {
        return start = currentTimeMillis();
    }

    public long getElapsedTime() {
        return currentTimeMillis() - start;
    }
}
