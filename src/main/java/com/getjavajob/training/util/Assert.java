package com.getjavajob.training.util;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author vinerI
 */
public class Assert {
    public static void fail (String message) {
        throw new AssertionError(message);
    }



    public static void assertEquals(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String expected, String actual) {
        if (Objects.equals(expected, actual)) {
            System.out.println("        Test throw " + expected + " passed");
        } else {
            System.out.println("Failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, int[] expected, Object[] actual) {
        if (Objects.equals(Arrays.toString(expected), Arrays.toString(actual))) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + Arrays.toString(expected) + ", actual "
                    + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, Object[] expected, Object[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + Arrays.toString(expected) + ", actual "
                    + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, Object expected, Object actual) {
         if (Objects.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }
}
