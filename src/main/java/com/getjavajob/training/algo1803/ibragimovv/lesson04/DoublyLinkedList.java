package com.getjavajob.training.algo1803.ibragimovv.lesson04;

import java.util.ConcurrentModificationException;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * @author vinerI
 */
public class DoublyLinkedList<V> extends AbstractLinkedList<V> {
    private int size;
    private Element<V> first;
    private Element<V> last;
    private int modCount;

    DoublyLinkedList() {
        this.size = 0;
    }

    DoublyLinkedList(int size) {
        this.size = size;
    }

    private void checkIndexOutOfBounds(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Index off");
        }
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean add(V value) {
        if (isEmpty()) {
            Element<V> element = new Element<>(null, null, value);
            first = element;
            last = element;
        } else {
            Element<V> element = new Element<>(last, null, value);
            last.setNext(element);
            last = element;
        }
        size++;
        modCount++;
        return true;
    }

    @Override
    public void add(int index, V value) {
        checkIndexOutOfBounds(index);
        if (index == size) {
            Element<V> l = last;
            Element<V> newElement = new Element<>(l, null, value);
            last = newElement;
            if (l == null) {
                first = newElement;
                } else {
                l.next = newElement;
                }
        } else if (index == 0) {
            Element<V> element = new Element<>(value);
            element.next = first;
            first = element;
            size++;
            modCount++;
        } else {
            Element<V> currentElement = first;
            while (--index != 0) {
                currentElement = currentElement.next;
            }
            Element<V> nextElement = currentElement.next;
            Element<V> insertedElement = new Element<>(value);
            insertedElement.next = nextElement;
            currentElement.next = insertedElement;
            size++;
            modCount++;
        }
    }

    @Override
    public V get(int index) {
        checkIndexOutOfBounds(index);
        Element<V> currentElement = first;
        if (index < size / 2) {
            Element<V> element = first;
            for (int i = 0; i < size; i++) {
                if (i == index) {
                    currentElement = element;
                    break;
                }
                element = element.next;
            }
        } else if (index >= size / 2) {
            Element<V> element = last;
            for (int i = size - 1; i > 0; i--) {
                if (i == index) {
                    currentElement = element;
                    break;
                }
                element = element.prev;
            }
        }
        return currentElement.val;
    }

    @Override
    public V remove(int index) {
        checkIndexOutOfBounds(index);
        Element<V> currentElement = first;
        if (index == 0) {
            first = currentElement.next;
        } else if (index == size - 1) {
            currentElement = last;
            last = currentElement.prev;
            currentElement.prev.setNext(null);
        } else {
            while (--index != 0) {
                currentElement = currentElement.next;
            }
            Element<V> removedElement = currentElement.next;
            currentElement.next = removedElement.next;
        }
        size--;
        modCount++;
        return currentElement.val;
    }

    @Override
    public boolean remove(Object element) {
        int removedElement = indexOf(element);
        if (removedElement == -1) {
            return false;
        }
        remove(removedElement);
        return true;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int indexOf(Object e) {
        if (isEmpty()) {
            throw new NoSuchElementException("No elements");
        }
        Element<V> element = first;
        for (int i = 0; i < size; i++) {
            if (element.val.equals(e)) {
                return i;
            }
            element = element.next;
        }
        return -1;
    }

    @Override
    public boolean contains (Object element) {
        return indexOf(element) >= 0;
    }

    @Override
    public Object[] toArray () {
        Object[] array = new Object[size];
        int i = 0;
        for (Element<V> element = first; element != null; element = element.next) {
            array[i++] = element.val;
        }
        return array;
    }

    public ListIteratorImpl listIterator() {
        return new ListIteratorImpl(0);
    }

    public ListIteratorImpl listIterator(int index) {
        checkIndexOutOfBounds(index);
        return new ListIteratorImpl(index);
    }

    public class Element<V> {
        private Element<V> prev;
        private Element<V> next;
        private V val;

        public Element(V val) {
            this.val = val;
        }

        public Element(Element<V> prev, Element<V> next, V val) {
            this.prev = prev;
            this.next = next;
            this.val = val;
        }

        void setNext(Element<V> next) {
            this.next = next;
        }
    }

    public class ListIteratorImpl implements ListIterator<V> {

        private Element<V> lastReturnedElement;
        private Element<V> currentElement = (Element<V>) first;
        private int index;
        private int expectedModCount = modCount;

        public ListIteratorImpl(int index) {
            this.index = index;
        }

        @Override
        public boolean hasNext() {
            return index < size;
        }

        @Override
        public V next() {
            checkSuchNextElement();
            lastReturnedElement = currentElement;
            V element = currentElement.val;
            currentElement = currentElement.next;
            index++;
            return element;
        }

        @Override
        public boolean hasPrevious() {
            return index > 0;
        }

        @Override
        public V previous() {
            checkSuchPreviousElement();
            currentElement = currentElement.prev;
            index--;
            lastReturnedElement = currentElement;
            return currentElement.val;
        }

        @Override
        public int nextIndex() {
            return hasNext() ? index + 1 : size - 1;
        }

        @Override
        public int previousIndex() {
            return hasPrevious() ? index - 1 : 0;
        }


        @Override
        public void add(V value) {
            checkConcurrentModification();
            Element<V> element = currentElement.prev;
            Element<V> insertedElement = new Element<>(value);
            Element<V> currentElement = this.currentElement;
            element.next = insertedElement;
            insertedElement.next = currentElement;
            currentElement.prev = insertedElement;
            insertedElement.prev = element;
            size++;
            index++;
            lastReturnedElement = null;
            modCount++;
            expectedModCount++;
        }

        @Override
        public void remove() {
            checkLastReturnedElement();
            checkConcurrentModification();
            Element<V> removedElement = lastReturnedElement.prev;
            Element<V> currentElement = lastReturnedElement.next;
            removedElement.next = currentElement;
            currentElement.prev = removedElement;
            size--;
            if (this.currentElement == lastReturnedElement) {
                this.currentElement = currentElement;
            } else {
                index--;
            }
            lastReturnedElement = null;
            expectedModCount++;
        }

        @Override
        public void set(V value) {
            checkLastReturnedElement();
            checkConcurrentModification();
            lastReturnedElement.val = value;
        }

        public void checkSuchNextElement() {
            if (!hasNext()) {
                throw new NoSuchElementException("No more elements");
            }
        }

        public void checkSuchPreviousElement() {
            if (!hasPrevious()) {
                throw new NoSuchElementException("Haven't previous element");
            }
        }

        public void checkLastReturnedElement() {
            if (lastReturnedElement == null) {
                throw new IllegalStateException("Can't remove element");
            }
        }

        final void checkConcurrentModification() {
            if (modCount != expectedModCount) {
                throw new ConcurrentModificationException();
            }
        }
    }
}
