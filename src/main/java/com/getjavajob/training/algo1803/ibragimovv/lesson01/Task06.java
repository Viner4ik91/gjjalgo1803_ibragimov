package com.getjavajob.training.algo1803.ibragimovv.lesson01;

/**
 * @author vinerI
 */
public class Task06 {
    public static void main(String[] args) {
        System.out.println(solveA(0b10));
        System.out.println(solveB(0b10, 0b10));
        System.out.println(solveC(0b11101, 4));
        System.out.println(solveD(0b1001, 2));
        System.out.println(solveE(0b1111, 4));
        System.out.println(solveF(0b1111, 2));
        System.out.println(solveG(0b11111, 4));
        System.out.println(solveH(0b11111, 4));
        System.out.println(solveI(5));
    }

    /**
     * a) given n<31, calc 2^n
     *
     * @param n - power
     * @return integer result
     */
    public static int solveA(int n) {
        int result = 0;
        if (n > 0 && n < 31) {
            result = 1 << n;
        } else {
            System.out.println("Sorry, 0 < n < 31, try again");
        }
        return result;
    }

    /**
     * b) given n,m<31, calc 2^n+2^m
     *
     * @param n - power
     * @param m - power
     * @return integer result
     */
    public static int solveB(int n, int m) {
        int result = 0;
        if (n > 0 && n < 31 && m > 0 && m < 31) {
            if (n == m) {
                result = ((1 << n) | (1 << m)) << 1;
            } else {
                result = (1 << n) | (1 << m);
            }
        } else {
            System.out.println("Sorry, 0 < n, m < 31, try again");
        }
        return result;
    }

    /**
     * c) given int a, n>0, reset n lower bits (create mask with n lower bits reset)
     *
     * @param n - power
     * @param a - integer number
     * @return integer result
     */
    public static int solveC(int a, int n) {
        int result = 0;
        if (n > 0) {
            result = a & (~1 << (n - 1));
        } else {
            System.out.println("Sorry, n > 0, try again");
        }
        return result;
    }

    /**
     * d) set a's n-th bit with 1
     *
     * @param a - integer number
     * @param n - bit
     * @return integer result
     */
    public static int solveD(int a, int n) {
        int result = 0;
        if (n > 0) {
            result = a | 1 << n;
        } else {
            System.out.println("Sorry, n > 0, try again");
        }
        return result;
    }

    /**
     * e) invert n-th bit (use 2 bit ops)
     *
     * @param a - integer number
     * @param n - bit
     * @return integer inverted result
     */
    public static int solveE(int a, int n) {
        int result = 0;
        if (n > 0) {
            result = a ^ 1 << n;
        } else {
            System.out.println("Sorry, n > 0, try again");
        }
        return result;
    }

    /**
     * f) set a's n-th bit with 0
     *
     * @param a - integer number
     * @param n - bit
     * @return integer a with n-th bit 0
     */
    public static int solveF(int a, int n) {
        int result = 0;
        if (n > 0) {
            result = a & ~(1 << n);
        } else {
            System.out.println("Sorry, n > 0, try again");
        }
        return result;
    }

    /**
     * g) return n lower bits
     *
     * @param a - integer number
     * @param n - bit
     * @return integer n lower bit
     */
    public static int solveG(int a, int n) {
        int result = 0;
        if (n > 0) {
            result = a & ~(~0 << n);
        } else {
            System.out.println("Sorry, n > 0, try again");
        }
        return result;
    }

    /**
     * h) return n-th bit
     *
     * @param a - integer number
     * @param n - bit
     * @return integer result
     */
    public static int solveH(int a, int n) {
        int result = 0;
        if (n > 0) {
            result = a & ~(~0 << n);
        } else {
            System.out.println("Sorry, n > 0, try again");
        }
        return result;
    }

    /**
     * i) given byte a. output bin representation using bit ops (don't use jdk api)
     *
     * @param a - byte
     * @return bin representation of a
     */
    public static String solveI(int a) {
        StringBuilder binRepresentation = new StringBuilder("0B");
        while (a > 0) {
            if (a % 2 == 0) {
                binRepresentation.append("0");
                a /= 2;
            } else {
                binRepresentation.append("1");
                a /= 2;
            }
        }
        return binRepresentation.toString();
    }
}
