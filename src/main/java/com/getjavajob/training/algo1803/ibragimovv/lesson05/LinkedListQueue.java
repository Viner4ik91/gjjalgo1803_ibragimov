package com.getjavajob.training.algo1803.ibragimovv.lesson05;

import com.getjavajob.training.algo1803.ibragimovv.lesson05.SinglyLinkedList.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * @author vinerI
 */
public class LinkedListQueue<E> extends AbstractQueue<E> {
    private Node<E> first;
    private Node<E> last;

    @Override
    public boolean add(E e) {
        Node<E> newElement = new Node<>();
        newElement.val = e;
        if (first == null) {
            first = newElement;
        } else {
            last.next = newElement;
        }
        last = newElement;
        return true;
    }

    @Override
    public E remove() {
        if (first == null) {
            throw new NoSuchElementException("Empty list");
        }
        E removableElement = first.val;
        if (first.next == null) {
            last = null;
        }
        first = first.next;
        return removableElement;
    }

    public List<E>  asList() {
        List<E> list = new ArrayList<>();
        Node<E> element = first;
        while (element != null) {
            list.add(element.val);
            element = element.next;
        }
        return list;
    }
}
