package com.getjavajob.training.algo1803.ibragimovv.lesson04;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class ListTest {
    public static void main(String[] args) {
        testAdd();
        testAddAll();
        testGet();
        testLastIndexOf();
        testListIterator();
        testRemove();
        testSet();
        testSubList();
    }

    private static void testAdd() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            list.add(i);
        }
        list.add(0, 3);
        int[] expected = {3, 0, 1};
        assertEquals("ListTest.testAdd", expected, list.toArray());
    }

    private static void testAddAll() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            list.add(i);
        }
        List<Integer> array = new ArrayList<>();
        for (int i = 2; i < 4; i++) {
            array.add(i);
        }
        list.addAll(2, array);
        int[] expected = {0, 1, 2, 3};
        assertEquals("ListTest.testAddAll", expected, list.toArray());
    }

    private static void testGet() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            list.add(i);
        }
        assertEquals("ListTest.testGet", 2, list.get(2));
    }

    private static void testLastIndexOf() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
        assertEquals("ListTest.testLastIndexOf", 4, list.lastIndexOf(4));
    }

    private static void testListIterator() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            list.add(i);
        }
        Iterator<Integer> iterator = list.iterator();

        assertEquals("ListTest.testIterator", 0, iterator.next());
    }

    private static void testRemove() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            list.add(i);
        }
        list.remove(5);
        int[] expected = {0, 1, 2, 3, 4};
        assertEquals("ListTest.testRemove", expected, list.toArray());
    }

    private static void testSet() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
        list.set(2, 55);
        int[] expected = {0, 1, 55, 3, 4};
        assertEquals("ListTest.testSet", expected, list.toArray());
    }

    private static void testSubList() {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            list.add(i);
        }
        int[] expected = {0, 1, 2, 3, 4};
        assertEquals("ListTest.testSubList", expected, list.subList(0, 5).toArray());
    }
}
