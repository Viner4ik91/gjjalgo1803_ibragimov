package com.getjavajob.training.algo1803.ibragimovv.lesson06;

import static java.lang.Integer.MAX_VALUE;
import static java.lang.Math.abs;
import static java.lang.Math.min;

/**
 * @author vinerI
 */
public class AssociativeArray<K, V> {
    private static final int DEFAULT_INITIAL_CAPACITY = 16;
    private static final int MAX_CAPACITY = 1 << 30;
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;

    private Entry[] table = new Entry[DEFAULT_INITIAL_CAPACITY];
    private int threshold = (int) (DEFAULT_INITIAL_CAPACITY * DEFAULT_LOAD_FACTOR);
    private int size;

    public V add(K key, V val) {
        int hash = hash(key);
        int index = indexFor(hash, table.length);
        Entry entry = table[index];
        for (; entry != null; entry = entry.next) {
            if (entry.key == key || entry.key.equals(key)) {
                return (V) entry.setValue(val);
            }
        }
        checkTableSize(index);
        table[index] = new Entry(key, val, table[index]);
        size++;
        return null;
    }

    public V get(K key) {
        int hash = hash(key);
        Entry entry = table[indexFor(hash, table.length)];
        for (; entry != null; entry = entry.next) {
            if (entry.key == key || entry.key.equals(key)) {
                return (V) entry.getValue();
            }
        }
        return null;
    }

    public V remove(K key) {
        int hash = hash(key);
        int tableIndex = indexFor(hash, table.length);
        Entry prev = table[tableIndex];
        Entry next;
        for (Entry element = prev; element != null; element = next) {
            next = element.next;
            if (element.key == key || element.key.equals(key)) {
                if (prev == element) {
                    table[tableIndex] = next;
                } else {
                    prev.next = next;
                }
                return (V) element.getValue();
            }
            prev = element;
        }
        return null;
    }

    private int hash(K key) {
        return (key == null) ? 0 : key.hashCode();
    }

    private int indexFor(int hash, int tableLength) {
        return abs(hash % tableLength);
    }

    private void checkTableSize(int bucketIndex) {
        if ((size >= threshold) && (table[bucketIndex] != null)) {
            resizeArray(2 * table.length);
        }
    }

    private void resizeArray(int newCapacity) {
        if (table.length == MAX_CAPACITY) {
            threshold = MAX_VALUE;
        }
        Entry[] oldTable = table;
        table = new Entry[newCapacity];
        reindex(oldTable);
        threshold = (int) min(newCapacity * DEFAULT_LOAD_FACTOR, MAX_CAPACITY + 1);
    }

    private void reindex(Entry[] table) {
        for (Entry element : table) {
            while (element != null) {
                add((K) element.getKey(), (V) element.getValue());
                element = element.next;
            }
        }
    }

    public class Entry<K, V> {
        K key;
        V value;
        Entry<K, V> next;

        public Entry(K key, V value, Entry<K, V> next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public V setValue(V value) {
            V oldValue = this.value;
            this.value = value;
            return oldValue;
        }
    }
}
