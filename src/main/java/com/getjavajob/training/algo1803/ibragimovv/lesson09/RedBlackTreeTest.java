package com.getjavajob.training.algo1803.ibragimovv.lesson09;

import com.getjavajob.training.algo1803.ibragimovv.lesson07.Node;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class RedBlackTreeTest {
    public static void main(String[] args) {
        testBalanceAdding();
        testBalanceRemoving();
    }

    private static void testBalanceAdding() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        tree.addRoot(10);
        tree.add(tree.root(), 9);
        tree.add(tree.root(), 8);
        tree.add(tree.root(), 7);
        tree.add(tree.root(), 6);
        tree.add(tree.root(), 11);
        tree.add(tree.root(), 12);
        assertEquals("RedBlackTreeTest.testBalanceAdding", "(9(7(6, 8), 11(10, 12)))", tree.toStringImpl());
    }

    private static void testBalanceRemoving() {
        RedBlackTree<Integer> tree = new RedBlackTree<>();
        tree.addRoot(10);
        tree.add(tree.root(), 9);
        tree.add(tree.root(), 8);
        tree.add(tree.root(), 7);
        tree.add(tree.root(), 6);
        Node<Integer> removed = tree.add(tree.root(), 11);
        tree.add(tree.root(), 12);
        tree.remove(removed);
        assertEquals("RedBlackTreeTest.testBalanceRemoving", "(9(7(6, 8), 10(null, 12)))", tree.toStringImpl());
    }
}
