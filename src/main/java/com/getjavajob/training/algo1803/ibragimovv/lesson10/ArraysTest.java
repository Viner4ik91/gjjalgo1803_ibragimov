package com.getjavajob.training.algo1803.ibragimovv.lesson10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static com.getjavajob.training.util.Assert.assertEquals;
import static java.util.Arrays.asList;
import static java.util.Arrays.binarySearch;
import static java.util.Arrays.copyOf;
import static java.util.Arrays.copyOfRange;
import static java.util.Arrays.deepEquals;
import static java.util.Arrays.fill;
import static java.util.Arrays.sort;

/**
 * @author vinerI
 */
public class ArraysTest {
    public static void main(String[] args) {
        testSortInt();
        testSortIntFromTo();
        testSortLong();
        testSortLongFromTo();
        testSortShort();
        testSortShortFromTo();
        testSortChar();
        testSortCharFromTo();
        testSortByte();
        testSortByteFromTo();
        testSortFloat();
        testSortFloatFromTo();
        testSortDouble();
        testSortDoubleFromTo();
        testSortObject();
        testSortObjectFromTo();
        testSortObjectComparator();
        testSortObjectComparatorFromTo();
        testBinarySearchInt();
        testBinarySearchIntFromTo();
        testEqualsTrue();
        testEqualsFalse();
        testFill();
        testFillFromTo();
        testCopyOf();
        testCopyOfRange();
        testAsList();
        testHashCode();
        testDeepHashCode();
        testDeepEquals();
    }

    private static void testSortInt() {
        int[] array = new int[]{3, 7, 4, 9, 5, 2, 6, 1};
        int[] expected = new int[]{1, 2, 3, 4, 5, 6, 7, 9};
        sort(array);
        assertEquals("ArraysTest.testSortInt", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortIntFromTo() {
        int[] array = new int[]{3, 7, 4, 9, 5, 2, 6, 1};
        int[] expected = new int[]{3, 7, 2, 4, 5, 9, 6, 1};
        sort(array, 2, 6);
        assertEquals("ArraysTest.testSortIntFromTo", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortLong() {
        long[] array = new long[]{3333333333L, 7777777777L, 4444444444L, 9999999999L, 5555555555L};
        long[] expected = new long[]{3333333333L, 4444444444L, 5555555555L, 7777777777L, 9999999999L};
        sort(array);
        assertEquals("ArraysTest.testSortLong", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortLongFromTo() {
        long[] array = new long[]{5555555555L, 7777777777L, 4444444444L, 9999999999L, 3333333333L};
        long[] expected = new long[]{5555555555L, 4444444444L, 7777777777L, 9999999999L, 3333333333L};
        sort(array, 1, 4);
        assertEquals("ArraysTest.testSortLongFromTo", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortShort() {
        short[] array = new short[]{3, 7, 4, 9, 5, 2, 6, 1};
        short[] expected = new short[]{1, 2, 3, 4, 5, 6, 7, 9};
        sort(array);
        assertEquals("ArraysTest.testSortShort", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortShortFromTo() {
        short[] array = new short[]{3, 7, 4, 9, 5, 2, 6, 1};
        short[] expected = new short[]{3, 7, 2, 4, 5, 9, 6, 1};
        sort(array, 2, 6);
        assertEquals("ArraysTest.testSortShortFromTo", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortChar() {
        char[] array = new char[]{'d', 'g', 'a', 'f', 'c', 'b', 'e'};
        char[] expected = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g'};
        sort(array);
        assertEquals("ArraysTest.testSortChar", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortCharFromTo() {
        char[] array = new char[]{'d', 'g', 'a', 'f', 'c', 'b', 'e'};
        char[] expected = new char[]{'d', 'g', 'a', 'b', 'c', 'f', 'e'};
        sort(array, 2, 6);
        assertEquals("ArraysTest.testSortCharFromTo", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortByte() {
        byte[] array = new byte[]{3, 7, 4, 9, 5, 2, 6, 1};
        byte[] expected = new byte[]{1, 2, 3, 4, 5, 6, 7, 9};
        sort(array);
        assertEquals("ArraysTest.testSortByte", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortByteFromTo() {
        byte[] array = new byte[]{3, 7, 4, 9, 5, 2, 6, 1};
        byte[] expected = new byte[]{3, 7, 2, 4, 5, 9, 6, 1};
        sort(array, 2, 6);
        assertEquals("ArraysTest.testSortByteFromTo", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortFloat() {
        float[] array = new float[]{3.1f, 7.1f, 4.1f, 9.1f, 5.1f, 2.1f, 6.1f, 1.1f};
        float[] expected = new float[]{1.1f, 2.1f, 3.1f, 4.1f, 5.1f, 6.1f, 7.1f, 9.1f};
        sort(array);
        assertEquals("ArraysTest.testSortFloat", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortFloatFromTo() {
        float[] array = new float[]{3.1f, 7.1f, 4.1f, 9.1f, 5.1f, 2.1f, 6.1f, 1.1f};
        float[] expected = new float[]{3.1f, 7.1f, 2.1f, 4.1f, 5.1f, 9.1f, 6.1f, 1.1f};
        sort(array, 2, 6);
        assertEquals("ArraysTest.testSortFloatFromTo", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortDouble() {
        double[] array = new double[]{3., 7., 4., 9., 5., 2., 6., 1.};
        double[] expected = new double[]{1., 2., 3., 4., 5., 6., 7., 9.};
        sort(array);
        assertEquals("ArraysTest.testSortDouble", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortDoubleFromTo() {
        double[] array = new double[]{3., 7., 4., 9., 5., 2., 6., 1.};
        double[] expected = new double[]{3., 7., 2., 4., 5., 9., 6., 1.};
        sort(array, 2, 6);
        assertEquals("ArraysTest.testSortDoubleFromTo", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortObject() {
        Object[] array = new Integer[]{3, 7, 4, 9, 5, 2, 6, 1};
        int[] expected = new int[]{1, 2, 3, 4, 5, 6, 7, 9};
        sort(array);
        assertEquals("ArraysTest.testSortObject", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortObjectFromTo() {
        Object[] array = new Integer[]{3, 7, 4, 9, 5, 2, 6, 1};
        int[] expected = new int[]{3, 7, 2, 4, 5, 9, 6, 1};
        sort(array, 2, 6);
        assertEquals("ArraysTest.testSortObjectFromTo", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortObjectComparator() {
        Integer[] array = new Integer[]{3, 7, 4, 9, 5, 2, 6, 1};
        int[] expected = new int[]{9, 7, 6, 5, 4, 3, 2, 1};
        Comparator<Integer> c = Comparator.reverseOrder();
        sort(array, c);
        assertEquals("ArraysTest.testSortObjectComparator", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testSortObjectComparatorFromTo() {
        Integer[] array = new Integer[]{3, 7, 4, 9, 5, 2, 6, 1};
        int[] expected = new int[]{3, 7, 2, 4, 5, 9, 6, 1};
        Comparator<Integer> c = Comparator.naturalOrder();
        sort(array, 2, 6, c);
        assertEquals("ArraysTest.testSortObjectComparatorFromTo", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testBinarySearchInt() {
        int[] array = new int[]{3, 7, 4, 9, 5, 2, 6, 1};
        assertEquals("ArraysTest.testBinarySearchInt", 3, binarySearch(array, 9));
    }

    private static void testBinarySearchIntFromTo() {
        int[] array = new int[]{3, 7, 4, 9, 5, 2, 6, 1};
        assertEquals("ArraysTest.testBinarySearchIntFromTo", 3, binarySearch(array, 2, 6, 9));
    }

    private static void testEqualsTrue() {
        int[] array = new int[]{3, 7, 4, 9, 5, 2, 6, 1};
        int[] secondArray = new int[]{3, 7, 4, 9, 5, 2, 6, 1};
        assertEquals("ArraysTest.testEqualsTrue", true, Arrays.equals(array, secondArray));
    }

    private static void testEqualsFalse() {
        int[] array = new int[]{3, 7, 4, 9, 5, 2, 6, 1};
        int[] secondArray = new int[]{2, 6, 1};
        assertEquals("ArraysTest.testEqualsFalse", false, Arrays.equals(array, secondArray));
    }

    private static void testFill() {
        int[] array = new int[5];
        int[] expected = new int[]{2, 2, 2, 2, 2};
        fill(array, 2);
        assertEquals("ArraysTest.testFill", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testFillFromTo() {
        int[] array = new int[5];
        int[] expected = new int[]{0, 2, 2, 2, 0};
        fill(array, 1, 4, 2);
        assertEquals("ArraysTest.testFillFromTo", Arrays.toString(expected), Arrays.toString(array));
    }

    private static void testCopyOf() {
        int[] array = new int[]{2, 2, 2, 2, 2};
        int[] actual = copyOf(array, array.length);
        int[] expected = new int[]{2, 2, 2, 2, 2};
        assertEquals("ArraysTest.testCopyOf", Arrays.toString(expected), Arrays.toString(actual));
    }

    private static void testCopyOfRange() {
        int[] array = new int[]{2, 2, 2, 2, 2};
        int[] actual = copyOfRange(array, 1, array.length);
        int[] expected = new int[]{2, 2, 2, 2};
        assertEquals("ArraysTest.testCopyOfRange", Arrays.toString(expected), Arrays.toString(actual));
    }

    private static void testAsList() {
        Integer[] array = new Integer[]{2, 2, 2, 2, 2};
        List<Integer> actual = asList(array);
        List<Integer> expected = new ArrayList<>();
        expected.add(2);
        expected.add(2);
        expected.add(2);
        expected.add(2);
        expected.add(2);
        assertEquals("ArraysTest.testAsList", expected, actual);
    }

    private static void testHashCode() {
        int[] array = new int[]{2, 2};
        assertEquals("ArraysTest.testHashCode", 1025, Arrays.hashCode(array));
    }

    private static void testDeepHashCode() {
        Integer[] array = new Integer[]{2, 2};
        assertEquals("ArraysTest.testDeepHashCode", 1025, Arrays.deepHashCode(array));
    }

    private static void testDeepEquals() {
        int[][] array = new int[][]{
                {1, 1, 1, 1},
                {1, 1, 1, 1},
        };
        int[][] secondArray = copyOf(array, array.length);
        assertEquals("ArraysTest.testDeepEquals", true, deepEquals(array, secondArray));
    }
}
