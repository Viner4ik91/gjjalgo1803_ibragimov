package com.getjavajob.training.algo1803.ibragimovv.lesson06;

import java.util.LinkedHashMap;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class LinkedHashMapTest {
    public static void main(String[] args) {
        testPut();
        testPutDuplicate();
    }

    private static void testPut() {
        LinkedHashMap<String, Integer> map = new LinkedHashMap<>();
        map.put("one", 1);
        map.put("two", 2);
        map.put("three", 3);
        assertEquals("LinkedHashMapTest.testPut", "{one=1, two=2, three=3}", map.toString());
    }

    private static void testPutDuplicate() {
        LinkedHashMap<String, Integer> map = new LinkedHashMap<>();
        map.put("one", 1);
        map.put("two", 2);
        map.put("three", 3);
        assertEquals("LinkedHashMapTest.testPut", 1, map.put("one", 4));
    }
}
