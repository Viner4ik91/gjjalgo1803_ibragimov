package com.getjavajob.training.algo1803.ibragimovv.lesson07.binary;

import com.getjavajob.training.algo1803.ibragimovv.lesson07.Node;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class ArrayBinaryTreeTest {
    public static void main(String[] args) {
        testLeft();
        testRight();
        testAddLeft();
        testAddRight();
        testRoot();
        testParent();
        testAddRoot();
        testAdd();
        testSet();
        testRemove();
        testSize();
        testSibling();
    }

    private static void testLeft() {
        ArrayBinaryTree<Integer> array = new ArrayBinaryTree<>();
        Node<Integer> root = array.addRoot(1);
        array.addLeft(root, 2);
        assertEquals("ArrayBinaryTreeTest.testLeft", 2, array.left(root).getElement());
    }

    private static void testRight() {
        ArrayBinaryTree<Integer> array = new ArrayBinaryTree<>();
        Node<Integer> root = array.addRoot(1);
        array.addRight(root, 3);
        assertEquals("ArrayBinaryTreeTest.testRight", 3, array.right(root).getElement());
    }

    private static void testAddLeft() {
        ArrayBinaryTree<Integer> array = new ArrayBinaryTree<>();
        Node<Integer> root = array.addRoot(1);
        assertEquals("ArrayBinaryTreeTest.testAddLeft", 2, array.addLeft(root, 2).getElement());
    }

    private static void testAddRight() {
        ArrayBinaryTree<Integer> array = new ArrayBinaryTree<>();
        Node<Integer> root = array.addRoot(1);
        assertEquals("ArrayBinaryTreeTest.testAddRight", 3, array.addRight(root, 3).getElement());
    }

    private static void testRoot() {
        ArrayBinaryTree<Integer> array = new ArrayBinaryTree<>();
        array.addRoot(1);
        assertEquals("ArrayBinaryTreeTest.testRoot", 1, array.root().getElement());
    }

    private static void testParent() {
        ArrayBinaryTree<Integer> array = new ArrayBinaryTree<>();
        Node<Integer> root = array.addRoot(1);
        Node<Integer> child = array.addLeft(root, 2);
        assertEquals("ArrayBinaryTreeTest.testParent", 1, array.parent(child).getElement());
    }

    private static void testAddRoot() {
        ArrayBinaryTree<Integer> array = new ArrayBinaryTree<>();
        assertEquals("ArrayBinaryTreeTest.testAddRoot", 10, array.addRoot(10).getElement());
    }

    private static void testAdd() {
        ArrayBinaryTree<Integer> array = new ArrayBinaryTree<>();
        Node<Integer> root = array.addRoot(1);
        array.add(root, 2);
        array.add(root, 3);
        assertEquals("ArrayBinaryTreeTest.testAdd", 2, array.left(root).getElement());
    }

    private static void testSet() {
        ArrayBinaryTree<Integer> array = new ArrayBinaryTree<>();
        Node<Integer> root = array.addRoot(1);
        Node<Integer> leftChild = array.add(root, 2);
        array.add(root, 3);
        array.set(leftChild, 20);
        assertEquals("ArrayBinaryTreeTest.testSet", 20, array.left(root).getElement());
    }

    private static void testRemove() {
        ArrayBinaryTree<Integer> array = new ArrayBinaryTree<>();
        Node<Integer> root = array.addRoot(1);
        array.addLeft(root, 2);
        Node<Integer> rightChild = array.addRight(root, 3);
        Node<Integer> leftOfRightChild = array.addLeft(rightChild, 4);
        array.remove(leftOfRightChild);
        assertEquals("ArrayBinaryTreeTest.testRemove", null, array.left(rightChild));
    }

    private static void testSize() {
        ArrayBinaryTree<Integer> array = new ArrayBinaryTree<>();
        Node<Integer> root = array.addRoot(1);
        Node<Integer> leftChild = array.addLeft(root, 2);
        Node<Integer> rightChild = array.addRight(root, 3);
        array.addLeft(rightChild, 4);
        array.remove(leftChild);
        assertEquals("ArrayBinaryTreeTest.testSize", 3, array.size());
    }

    private static void testSibling() {
        ArrayBinaryTree<Integer> array = new ArrayBinaryTree<>();
        Node<Integer> root = array.addRoot(1);
        Node<Integer> left = array.addLeft(root, 2);
        Node<Integer> right = array.addRight(root, 3);
        assertEquals("ArrayBinaryTreeTest.testSibling", right, array.sibling(left));
    }
}
