package com.getjavajob.training.algo1803.ibragimovv.lesson07.binary;

import com.getjavajob.training.algo1803.ibragimovv.lesson07.Node;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class ArrayBinaryTree<E> extends AbstractBinaryTree<E> {
    private List<Node<E>> array = new ArrayList<>();
    private int size;

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        int childPosition = 2 * array.indexOf(p) + 1;
        if (childPosition <= 0) {
            throw new IllegalArgumentException("Out of array node");
        } else {
            return childPosition < array.size() ? array.get(childPosition) : null;
        }
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        int childPosition = 2 * array.indexOf(p) + 2;
        if (childPosition <= 0) {
            throw new IllegalArgumentException("Out of array node");
        } else {
            return childPosition < array.size() ? array.get(childPosition) : null;
        }
    }

    private void growArray(int index) {
        int size = size();
        if (index > size) {
            for (int i = size; i <= index; i++) {
                array.add(null);
            }
        } else if (index == 0) {
            array.add(null);
        }
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        int nodeIndex = array.indexOf(n);
        int childPosition = 2 * nodeIndex + 1;
        growArray(childPosition);
        if (nodeIndex < 0) {
            throw new IllegalArgumentException("Out of array node");
        } else if (childPosition < array.size() && array.get(childPosition) != null) {
            throw new IllegalArgumentException("Left child is already defined");
        }
        NodeImpl<E> leftNode = new NodeImpl<>(e);
        array.add(childPosition, leftNode);
        size++;
        return leftNode;
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        int nodeIndex = array.indexOf(n);
        int childPosition = 2 * nodeIndex + 2;
        growArray(childPosition);
        if (nodeIndex < 0) {
            throw new IllegalArgumentException("Out of array node");
        } else if (childPosition < array.size() && array.get(childPosition) != null) {
            throw new IllegalArgumentException("Right child is already defined ");
        }
        NodeImpl<E> rightNode = new NodeImpl<>(e);
        array.add(childPosition, rightNode);
        size++;
        return rightNode;
    }

    @Override
    public Node<E> root() {
        return array.size() == 0 ? null : array.get(0);
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        int indexNode = array.indexOf(n);
        if (indexNode < 0) {
            throw new IllegalArgumentException("Out of array node");
        }
        if (indexNode == 0) {
            return null;
        }
        return array.get((indexNode - 1) / 2);
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (root() != null) {
            throw new IllegalStateException("Root is already defined");
        }
        NodeImpl<E> root = new NodeImpl<>(e);
        array.add(0, root);
        size++;
        return root;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        if (left(n) == null) {
            return addLeft(n, e);
        } else if (right(n) == null) {
            return addRight(n, e);
        } else {
            throw new IllegalArgumentException("All childes is already defined");
        }
    }

    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        int indexNode = array.indexOf(n);
        if (indexNode < 0) {
            throw new IllegalArgumentException("Out of array node");
        }
        E replacedElement = n.getElement();
        NodeImpl<E> newElement = new NodeImpl<>(e);
        array.set(indexNode, newElement);
        return replacedElement;
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        int indexNode = array.indexOf(n);
        if (indexNode < 0) {
            throw new IllegalArgumentException("Out of array node");
        }
        if (left(n) != null || right(n) != null) {
            throw new IllegalArgumentException("Node has a child");
        }
        Node<E> replacedElement = array.set(indexNode, null);
        size--;
        return replacedElement.getElement();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            int position;

            @Override
            public boolean hasNext() {
                return position < size();
            }

            @Override
            public E next() {
                for (; position < size(); position++ ) {
                    while(array.get(position) != null) {
                        return array.get(position).getElement();
                    }
                }
                return null;
            }
        };
    }

    @Override
    public Collection<Node<E>> nodes() {
        Collection<Node<E>> allNodes = new ArrayList<>();
        for (Node<E> node : array) {
            while (node != null) {
                allNodes.add(node);
            }
        }
        return allNodes;
    }

    protected static class NodeImpl<E> implements Node<E> {
        private E val;

        public NodeImpl(E val) {
            this.val = val;
        }

        @Override
        public E getElement() {
            return val;
        }
    }
}
