package com.getjavajob.training.algo1803.ibragimovv.lesson04;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vinerI
 */
public class SinglyLinkedList<E> {
    private Node<E> head;

    public void add(E e) {
        Node<E> newElement = new Node<>();
        newElement.val = e;
        Node<E> currentElement = head;
        if (head == null) {
            head = newElement;
        } else {
            while (currentElement.next != null) {
                currentElement = currentElement.next;
            }
            currentElement.next = newElement;
        }
    }

    public E get(int index) {
        Node<E> element = head;
        for (int i = 0; i < index; i++) {
            element = element.next;
        }
        return element.val;
    }

    public int size() {
        int size = 0;
        Node<E> element = head;
        while (element != null) {
            size++;
            element = element.next;
        }
        return size;
    }

    public void reverse() {
        int reverseSize = size();
        for (int i = 0; i < reverseSize / 2; i++) {
            relink(i, reverseSize - 1 - i);
        }
    }

    public void relink(int index1, int index2) {
        if (size() > 0) {
            Node<E> element1 = head;
            for (int i = 0; i < index1; i++) {
                element1 = element1.next;
            }
            Node<E> element2 = head;
            for (int i = 0; i < index2; i++) {
                element2 = element2.next;
            }
            E saveElement1 = element1.val;
            element1.val = element2.val;
            element2.val = saveElement1;
        }
    }

    public List<E> asList() {
        List<E> list = new ArrayList();
        Node<E> currentElement = head;
        while (currentElement != null) {
            list.add(currentElement.val);
            currentElement = currentElement.next;
        }
        return list;
    }

    private static class Node<E> {
        private Node<E> next;
        private E val;
    }
}
