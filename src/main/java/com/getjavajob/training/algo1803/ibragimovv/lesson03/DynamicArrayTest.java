package com.getjavajob.training.algo1803.ibragimovv.lesson03;

import static com.getjavajob.training.util.Assert.assertEquals;
import static com.getjavajob.training.util.Assert.fail;

/**
 * @author vinerI
 */
public class DynamicArrayTest {
    private static final String FAIL_MESSAGE = "Test failed";
    private static final String INDEX_OUT_MESSAGE = "ArrayIndexOutOfBoundsException";

    public static void main(String[] args) {
        testBooleanAdd();
        testAddFromBegin();
        testAddFromMiddle();
        testAddFromEnd();
        testSet();
        testPrevElement();
        testGet();
        testRemoveFromBegin();
        testRemoveFromMiddle();
        testRemoveFromEnd();
        testRemovedElement();
        testBooleanRemove();
        testSize();
        testIndexOf();
        testContainsTrue();
        testContainsFalse();
        testToArray();
    }

    private static void testBooleanAdd() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }
        assertEquals("DynamicArray.testBooleanAdd", true, arrayData.add(5));
    }

    private static void testAddFromBegin() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }
        int[] expected = {1, 0, 1, 2, 3, 4};
        arrayData.add(0, 1);
        try {
            arrayData.add(7, 0);
            fail(FAIL_MESSAGE);
        } catch (Exception e) {
            assertEquals(INDEX_OUT_MESSAGE, e.getMessage());
        } catch (AssertionError e) {
            assertEquals(FAIL_MESSAGE, e.getMessage());
        }

        assertEquals("DynamicArray.testAddFromBegin", expected, arrayData.toArray());
    }

    private static void testAddFromMiddle() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }
        int[] expected = {0, 1, 2, 1, 3, 4};
        arrayData.add(3, 1);
        try {
            arrayData.add(8, 1);
            fail(FAIL_MESSAGE);
        } catch (Exception e) {
            assertEquals(INDEX_OUT_MESSAGE, e.getMessage());
        } catch (AssertionError e) {
            assertEquals(FAIL_MESSAGE, e.getMessage());
        }

        assertEquals("DynamicArray.testAddFromMiddle", expected, arrayData.toArray());
    }

    private static void testAddFromEnd() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }
        int[] expected = {0, 1, 2, 3, 4, 1};
        arrayData.add(1);
        try {
            arrayData.add(10, 1);
            fail(FAIL_MESSAGE);
        } catch (Exception e) {
            assertEquals(INDEX_OUT_MESSAGE, e.getMessage());
        } catch (AssertionError e) {
            assertEquals(FAIL_MESSAGE, e.getMessage());
        }

        assertEquals("DynamicArray.testAddFromEnd", expected, arrayData.toArray());
    }

    private static void testSet() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }
        int[] expected = {9, 1, 2, 3, 4};
        arrayData.set(0, 9);
        try {
            arrayData.add(15, 9);
            fail(FAIL_MESSAGE);
        } catch (Exception e) {
            assertEquals(INDEX_OUT_MESSAGE, e.getMessage());
        } catch (AssertionError e) {
            assertEquals(FAIL_MESSAGE, e.getMessage());
        }

        assertEquals("DynamicArray.testSet", expected, arrayData.toArray());
    }

    private static void testPrevElement() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }

        assertEquals("DynamicArray.testPrevElement", 4, (int)arrayData.set(4, 0));
    }

    private static void testGet() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }
        try {
            arrayData.get(15);
            fail(FAIL_MESSAGE);
        } catch (Exception e) {
            assertEquals(INDEX_OUT_MESSAGE, e.getMessage());
        } catch (AssertionError e) {
            assertEquals(FAIL_MESSAGE, e.getMessage());
        }

        assertEquals("DynamicArray.testGet", 4, (int)arrayData.get(4));
    }

    private static void testRemoveFromBegin() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }
        Integer[] expected = {1, 2, 3, 4};
        arrayData.remove(0);
        try {
            arrayData.remove(15);
            fail(FAIL_MESSAGE);
        } catch (Exception e) {
            assertEquals(INDEX_OUT_MESSAGE, e.getMessage());
        } catch (AssertionError e) {
            assertEquals(FAIL_MESSAGE, e.getMessage());
        }

        assertEquals("DynamicArray.testRemoveFromBegin", expected, arrayData.toArray());
    }

    private static void testRemoveFromMiddle() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }
        Integer[] expected = {0, 1, 2, 4};
        arrayData.remove(3);
        try {
            arrayData.remove(15);
            fail(FAIL_MESSAGE);
        } catch (Exception e) {
            assertEquals(INDEX_OUT_MESSAGE, e.getMessage());
        } catch (AssertionError e) {
            assertEquals(FAIL_MESSAGE, e.getMessage());
        }

        assertEquals("DynamicArray.testRemoveFromMiddle", expected, arrayData.toArray());
    }

    private static void testRemoveFromEnd() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }
        Integer[] expected = {0, 1, 2, 3};
        arrayData.remove(4);
        try {
            arrayData.remove(-2);
            fail(FAIL_MESSAGE);
        } catch (Exception e) {
            assertEquals(INDEX_OUT_MESSAGE, e.getMessage());
        } catch (AssertionError e) {
            assertEquals(FAIL_MESSAGE, e.getMessage());
        }

        assertEquals("DynamicArray.testRemoveFromEnd", expected, arrayData.toArray());
    }

    private static void testRemovedElement() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }

        assertEquals("DynamicArray.testRemovedElement", 1, (int)arrayData.remove(1));
    }

    private static void testBooleanRemove() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }
        assertEquals("DynamicArray.testBooleanRemove", true, arrayData.remove(Integer.valueOf(2)));
    }

    private static void testSize() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }
        assertEquals("DynamicArray.testSize", 5, arrayData.size());
    }

    private static void testIndexOf() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }
        assertEquals("DynamicArray.testIndexOf", 3, arrayData.indexOf(3));
    }

    private static void testContainsTrue() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }
        assertEquals("DynamicArray.testContainsTrue", true, arrayData.contains(4));
    }

    private static void testContainsFalse() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }
        assertEquals("DynamicArray.testContainsFalse", false, arrayData.contains(100));
    }

    private static void testToArray() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }
        int[]expected = {0, 1, 2, 3, 4};
        assertEquals("DynamicArray.testContainsFalse", expected, arrayData.toArray());
    }
}