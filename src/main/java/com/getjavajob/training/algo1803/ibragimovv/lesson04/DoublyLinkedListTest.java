package com.getjavajob.training.algo1803.ibragimovv.lesson04;

import static com.getjavajob.training.util.Assert.assertEquals;
import static com.getjavajob.training.util.Assert.fail;

/**
 * @author vinerI
 */
public class DoublyLinkedListTest {
    public static void main(String[] args) {
        testAddToBegin();
        testAddToPosition();
        testAddToEnd();
        testGet();
        testRemoveFirstElement();
        testRemoveToIndex();
        testRemoveLastElement();
        testRemoveBoolean();
    }

    private static DoublyLinkedList<Integer> initializeList() {
        DoublyLinkedList<Integer> linkedList = new DoublyLinkedList<>();
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(3);
        linkedList.add(4);
        linkedList.add(5);
        return linkedList;
    }

    private static void testAddToBegin() {
        DoublyLinkedList<Integer> linkedList = initializeList();
        linkedList.add(0, 10);
        try {
            linkedList.add(-1, 10);
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("Index off", e.getMessage());
        }

        assertEquals("DoublyLinkedList.testAddToBegin", new Integer[]{10, 1, 2, 3, 4, 5}, linkedList.toArray());
    }

    private static void testAddToPosition() {
        DoublyLinkedList<Integer> linkedList = initializeList();
        linkedList.add(3, 10);
        try {
            linkedList.add(15, 10);
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("Index off", e.getMessage());
        }

        assertEquals("DoublyLinkedList.testAddToPosition", new Integer[]{1, 2, 3, 10, 4, 5}, linkedList.toArray());
    }

    private static void testAddToEnd() {
        DoublyLinkedList<Integer> linkedList = initializeList();
        linkedList.add(10);
        assertEquals("DoublyLinkedList.testAddToEnd", new Integer[]{1, 2, 3, 4, 5, 10}, linkedList.toArray());
    }

    private static void testGet() {
        DoublyLinkedList<Integer> linkedList = initializeList();
        try {
            //noinspection ResultOfMethodCallIgnored
            linkedList.get(7);
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("Index off", e.getMessage());
        }
        assertEquals("DoublyLinkedList.testGet", 2, linkedList.get(1));
    }

    private static void testRemoveFirstElement() {
        DoublyLinkedList<Integer> linkedList = initializeList();
        linkedList.remove(0);

        assertEquals("DoublyLinkedList.testRemoveFirstElement", new Integer[]{2, 3, 4, 5}, linkedList.toArray());
    }

    private static void testRemoveToIndex() {
        DoublyLinkedList<Integer> linkedList = initializeList();
        linkedList.remove(2);
        try {
            linkedList.remove(100);
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("Index off", e.getMessage());
        }

        assertEquals("DoublyLinkedList.testRemoveToIndex", new Integer[]{1, 2, 4, 5}, linkedList.toArray());
    }

    private static void testRemoveLastElement() {
        DoublyLinkedList<Integer> linkedList = initializeList();
        linkedList.remove(linkedList.size() - 1);

        assertEquals("DoublyLinkedList.testRemoveLastElement", new Integer[]{1, 2, 3, 4}, linkedList.toArray());
    }

    private static void testRemoveBoolean() {
        DoublyLinkedList<Integer> linkedList = initializeList();
        Integer val = 5;
        assertEquals("DoublyLinkedList.testRemoveBoolean", true, linkedList.remove(val));
    }
}
