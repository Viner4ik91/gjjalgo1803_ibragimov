package com.getjavajob.training.algo1803.ibragimovv.lesson05;

import static com.getjavajob.training.util.Assert.assertEquals;
import static com.getjavajob.training.util.Assert.fail;

/**
 * @author vinerI
 */
public class LinkedListQueueTest {
    public static void main(String[] args) {
        testAdd();
        testRemove();
    }

    private static void testAdd() {
        LinkedListQueue<Integer> listQueue = new LinkedListQueue<>();
        for (int i = 0; i < 3; i++) {
            listQueue.add(i);
        }
        int[] expected = new int[] {0, 1, 2};
        assertEquals("LinkedListQueueTest.testAdd", expected, listQueue.asList().toArray());
    }

    private static void testRemove() {
        LinkedListQueue<Integer> listQueue = new LinkedListQueue<>();
        try {
            listQueue.remove();
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("NoSuchElementException", e.getClass().getSimpleName());
        }

        for (int i = 0; i < 3; i++) {
            listQueue.add(i);
        }
        listQueue.remove();
        listQueue.remove();
        int[] expected = new int[]{2};
        assertEquals("LinkedListQueueTest.testRemove", expected, listQueue.asList().toArray());
    }
}
