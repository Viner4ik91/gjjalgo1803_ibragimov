package com.getjavajob.training.algo1803.ibragimovv.lesson10;

/**
 * @author vinerI
 */
public class MergeSort {
    public void sort(int[] array) {
        int[] mergeArray = new int[array.length];
        splitArray(array,  0, array.length - 1, mergeArray);
    }

    private void splitArray(int[] array, int first, int last, int[] mergeArray) {
        if (last - first >= 1) {
            int middle = (first + last) >> 1;
            splitArray(array, first, middle, mergeArray);
            splitArray(array, middle + 1, last, mergeArray);
            mergeSorting(array, first, middle, last, mergeArray);
        }
    }

    private void mergeSorting(int[] array, int first, int middle, int last, int[] mergeArray) {
        int tempIndex = 0;
        int low = first;
        int mid = middle + 1;
        int allElements = last - first + 1;
        while (low <= middle || mid <= last) {
            if (low > middle) {
                mergeArray[tempIndex++] = array[mid++];
            } else if (mid > last) {
                mergeArray[tempIndex++] = array[low++];
            } else if (array[low] < array[mid]) {
                mergeArray[tempIndex++] = array[low++];
            } else {
                mergeArray[tempIndex++] = array[mid++];
            }
        }
        for (mid = 0; mid < allElements; mid++) {
            array[first + mid] = mergeArray[mid];
        }
    }

    //for bytes
    public void sort(byte[] array) {
        byte[] mergeArray = new byte[array.length];
        splitArray(array,  0, array.length - 1, mergeArray);
    }

    private void splitArray(byte[] array, int first, int last, byte[] mergeArray) {
        if (last - first >= 1) {
            int middle = (first + last) >> 1;
            splitArray(array, first, middle, mergeArray);
            splitArray(array, middle + 1, last, mergeArray);
            mergeSorting(array, first, middle, last, mergeArray);
        }
    }

    private void mergeSorting(byte[] array, int first, int middle, int last, byte[] mergeArray) {
        int tempIndex = 0;
        int low = first;
        int mid = middle + 1;
        int allElements = last - first + 1;
        while (low <= middle || mid <= last) {
            if (low > middle) {
                mergeArray[tempIndex++] = array[mid++];
            } else if (mid > last) {
                mergeArray[tempIndex++] = array[low++];
            } else if (array[low] < array[mid]) {
                mergeArray[tempIndex++] = array[low++];
            } else {
                mergeArray[tempIndex++] = array[mid++];
            }
        }
        for (mid = 0; mid < allElements; mid++) {
            array[first + mid] = mergeArray[mid];
        }
    }
}
