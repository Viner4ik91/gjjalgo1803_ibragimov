package com.getjavajob.training.algo1803.ibragimovv.lesson08.binary.search;

import com.getjavajob.training.algo1803.ibragimovv.lesson07.Node;
import com.getjavajob.training.algo1803.ibragimovv.lesson07.binary.LinkedBinaryTree;

import java.util.Comparator;

/**
 * @author vinerI
 */
public class BinarySearchTree<E> extends LinkedBinaryTree<E> {
    private Comparator<E> comparator;

    public BinarySearchTree() {
    }

    public BinarySearchTree(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    /**
     * Method for comparing two values
     *
     * @param val1
     * @param val2
     * @return
     */
    protected int compare(E val1, E val2) {
        if (val1 != null && val2 != null) {
            if (comparator != null) {
                return comparator.compare(val1, val2);
            }
            return ((Comparable<E>) val1).compareTo(val2);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Returns the node in n's subtree by val
     *
     * @param n
     * @param val
     * @return
     */
    public Node<E> treeSearch(Node<E> n, E val) {
        if (n == null) {
            return null;
        }
        NodeImpl<E> node = validate(n);
        int comparison = compare(node.getElement(), val);
        if (comparison == 0) {
            return node;
        } else if (comparison > 0) {
            return treeSearch(node.getLeftChild(), val);
        } else {
            return treeSearch(node.getRightChild(), val);
        }
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        Node<E> addedRoot = super.addRoot(e);
        afterElementAdded(addedRoot);
        return addedRoot;
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        Node<E> added;
        NodeImpl<E> node = validate(n);
        if (compare(node.getElement(), e) == 0) {
            return null;
        } else if (compare(n.getElement(), e) > 0) {
            if (node.getLeftChild() == null) {
                added = addLeft(node, e);
            } else {
                added = add(node.getLeftChild(), e);
            }
        } else {
            if (node.getRightChild() == null) {
                added = addRight(node, e);
            } else {
                added = add(node.getRightChild(), e);
            }
        }
        afterElementAdded(added);
        return added;
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        if (node.getLeftChild() == null && node.getRightChild() == null) {
            E deletedValue = super.remove(n);
            afterElementRemoved(node);
            return deletedValue;
        } else if (node.getLeftChild() == null || node.getRightChild() == null) {
            NodeImpl<E> parent = validate(node.getParent());
            if (parent != null) {
                if (node == parent.getLeftChild()) {
                    if (node.getLeftChild() != null) {
                        parent.setLeftChild(node.getLeftChild());
                        parent.getLeftChild().setParent(parent);
                    } else {
                        parent.setLeftChild(node.getRightChild());
                        parent.getLeftChild().setParent(parent);
                    }
                } else if (node == parent.getRightChild()) {
                    if (node.getLeftChild() != null) {
                        parent.setRightChild(node.getLeftChild());
                        parent.getRightChild().setParent(parent);
                    } else {
                        parent.setRightChild(node.getRightChild());
                        parent.getRightChild().setParent(parent);
                    }
                }
            }
            afterElementRemoved(node);
        } else {
            NodeImpl<E> minOfRight = minElement(node);
            E deleted = node.getElement();
            node.setElement(minOfRight.getElement());
            remove(minOfRight);
            return deleted;
        }
        afterElementRemoved(node);
        return node.getElement();
    }

    private NodeImpl<E> minElement(NodeImpl<E> node) {
        if (node.getLeftChild() == null) {
            return node;
        } else {
            return minElement(node.getLeftChild());
        }
    }

    public String toStringImpl() {
        StringBuilder sb = new StringBuilder();
        NodeImpl<E> node = validate(root());

        if (node != null) {
            sb.append("(").append(node.getElement());
            if (node.getLeftChild() != null) {
                NodeImpl<E> firstLeft = node.getLeftChild();
                sb.append("(").append(firstLeft.getElement());
                if (firstLeft.getLeftChild() != null) {
                    while (firstLeft.getLeftChild() != null) {
                        sb.append("(").append(firstLeft.getLeftChild().getElement());
                        if (firstLeft.getRightChild() != null) {
                            if (firstLeft.getRightChild().getLeftChild() != null && firstLeft.getRightChild().getRightChild() != null) {
                            sb.append(", ").append(firstLeft.getRightChild().getElement()).append("(")
                                    .append(firstLeft.getRightChild().getLeftChild().getElement()).append(",")
                                    .append(firstLeft.getRightChild().getRightChild().getElement()).append("))");
                            } else {
                            sb.append(", ").append(firstLeft.getRightChild().getElement()).append(")");
                            }
                        }

                        firstLeft = firstLeft.getLeftChild();
                    }
                } else if (firstLeft.getLeftChild() == null && firstLeft.getRightChild() != null) {
                    while (firstLeft.getRightChild() != null) {
                        sb.append("(null, ").append(firstLeft.getRightChild().getElement()).append(")");
                        firstLeft = firstLeft.getRightChild();
                    }
                } else {
                    sb.append(")");
                }
            }
            if (node.getRightChild() != null) {
                NodeImpl<E> firstRight = node.getRightChild();
                sb.append(", ").append(firstRight.getElement());
                if (firstRight.getLeftChild() != null) {
                    while (firstRight.getLeftChild() != null) {
                        sb.append("(").append(firstRight.getLeftChild().getElement());
                        if (firstRight.getRightChild() != null) {
                            sb.append(", ").append(firstRight.getRightChild().getElement()).append(")");
                        }
                        firstRight = firstRight.getLeftChild();
                    }
                } else if (firstRight.getLeftChild() == null && firstRight.getRightChild() != null) {
                    while (firstRight.getRightChild() != null) {
                        sb.append("(null, ").append(firstRight.getRightChild().getElement()).append("))");
                        firstRight = firstRight.getRightChild();
                    }
                } else {
                    sb.append(")");
                }
            }
            checkParenthesis(sb);
        }
        return sb.toString();
    }

    private void checkParenthesis(StringBuilder tree) {
        int k = 0;

        for (int i = 0; i < tree.length(); ++i) {
            switch (tree.charAt(i)) {
                case '(':
                    ++k;
                    break;
                case ')':
                    --k;
            }
        }

        if (k > 0) {
            for (int i =0; i < k; i ++) {
                tree.append(")");
            }
        }
    }

    protected void afterElementRemoved(Node<E> n) {

    }

    protected void afterElementAdded(Node<E> n) {

    }
}
