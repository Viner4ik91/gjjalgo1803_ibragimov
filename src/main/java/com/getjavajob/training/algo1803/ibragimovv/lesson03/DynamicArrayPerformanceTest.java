package com.getjavajob.training.algo1803.ibragimovv.lesson03;

import com.getjavajob.training.util.StopWatch;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vinerI
 */
@SuppressWarnings("ALL")
public class DynamicArrayPerformanceTest {
    private static final int MILLION_SIZE = 1_000_000;
    private static final int HALF_A_MILLION = MILLION_SIZE / 2;
    public static void main(String[] args) {
        testAddFromBegin();
        testJdkAddFromBegin();
        testAddFromMiddle();
        testJdkAddFromMiddle();
        testAddFromEnd();
        testJdkAddFromEnd();
        testRemoveFromBegin();
        testJdkRemoveFromBegin();
        testRemoveFromMiddle();
        testJdkRemoveFromMiddle();
        testRemoveFromEnd();
        testJdkRemoveFromEnd();
    }

    private static void testAddFromBegin() {
        DynamicArray array = new DynamicArray();
        for (int i = 0; i < MILLION_SIZE; i++) {
            array.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < 1000; i++) {
            array.add(0, i);
        }
        System.out.println("The time of the adding 1000 elements from begin: " + time.getElapsedTime() + "ms");
    }

    private static void testJdkAddFromBegin() {
        List<Integer> jdkArray = new ArrayList<>();
        for (int i = 0; i < MILLION_SIZE; i++) {
            jdkArray.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < 1000; i++) {
            jdkArray.add(0, i);
        }
        System.out.println("The time of the adding 1000 elements from begin: " + time.getElapsedTime() + "ms");
    }

    private static void testAddFromMiddle() {
        DynamicArray array = new DynamicArray();
        for (int i = 0; i < MILLION_SIZE; i++) {
            array.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < 2000; i++) {
            array.add(HALF_A_MILLION, i);
        }
        System.out.println("The time of the adding 2000 elements from the middle: " + time.getElapsedTime() + "ms");
    }

    private static void testJdkAddFromMiddle() {
        List<Integer> jdkArray = new ArrayList<>();
        for (int i = 0; i < MILLION_SIZE; i++) {
            jdkArray.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < 2000; i++) {
            jdkArray.add(HALF_A_MILLION, i);
        }
        System.out.println("The time of the adding 2000 elements from the middle: " + time.getElapsedTime() + "ms");
    }

    private static void testAddFromEnd() {
        DynamicArray array = new DynamicArray();
        for (int i = 0; i < MILLION_SIZE; i++) {
            array.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < MILLION_SIZE * 10; i++) {
            array.add(i);
        }
        System.out.println("The time of the adding 10_000_000 elements from the end: " + time.getElapsedTime() + "ms");
    }

    private static void testJdkAddFromEnd() {
        List<Integer> jdkArray = new ArrayList<>();
        for (int i = 0; i < MILLION_SIZE; i++) {
            jdkArray.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < MILLION_SIZE * 10; i++) {
            jdkArray.add(i);
        }
        System.out.println("The time of the adding 10_000_000 elements from the end: " + time.getElapsedTime() + "ms");
    }

    private static void testRemoveFromBegin() {
        DynamicArray array = new DynamicArray();
        for (int i = 0; i < MILLION_SIZE; i++) {
            array.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < 1000; i++) {
            array.remove(0);
        }
        System.out.println("The time of the removing 1000 elements from begin: " + time.getElapsedTime() + "ms");
    }

    private static void testJdkRemoveFromBegin() {
        List<Integer> jdkArray = new ArrayList<>();
        for (int i = 0; i < MILLION_SIZE; i++) {
            jdkArray.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < 1000; i++) {
            jdkArray.remove(0);
        }
        System.out.println("The time of the removing 1000 elements from begin: " + time.getElapsedTime() + "ms");
    }

    private static void testRemoveFromMiddle() {
        DynamicArray array = new DynamicArray();
        for (int i = 0; i < MILLION_SIZE; i++) {
            array.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < 2000; i++) {
            array.remove(HALF_A_MILLION);
        }
        System.out.println("The time of the removing 2000 elements from the middle: " + time.getElapsedTime() + "ms");
    }

    private static void testJdkRemoveFromMiddle() {
        List<Integer> jdkArray = new ArrayList<>();
        for (int i = 0; i < MILLION_SIZE; i++) {
            jdkArray.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < 2000; i++) {
            jdkArray.remove(HALF_A_MILLION);
        }
        System.out.println("The time of the removing 2000 elements from the middle: " + time.getElapsedTime() + "ms");
    }

    private static void testRemoveFromEnd() {
        DynamicArray array = new DynamicArray();
        for (int i = 0; i < MILLION_SIZE; i++) {
            array.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < 100000; i++) {
            array.remove(array.size() - 1 - i);
        }
        System.out.println("The time of the removing 100000 elements from the end: " + time.getElapsedTime() + "ms");
    }

    private static void testJdkRemoveFromEnd() {
        List<Integer> jdkArray = new ArrayList<>();
        for (int i = 0; i < MILLION_SIZE; i++) {
            jdkArray.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < 100000; i++) {
            jdkArray.remove(jdkArray.size() - 1 - i);
        }
        System.out.println("The time of the removing 100000 elements from the middle: " + time.getElapsedTime() + "ms");
    }
}
