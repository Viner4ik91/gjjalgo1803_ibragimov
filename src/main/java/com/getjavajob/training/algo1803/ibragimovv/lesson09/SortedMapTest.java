package com.getjavajob.training.algo1803.ibragimovv.lesson09;

import java.util.SortedMap;
import java.util.TreeMap;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class SortedMapTest {
    public static void main(String[] args) {
        testComparator();
        testSubMap();
        testHeadMap();
        testTailMap();
        testFirstKey();
        testLastKey();
        testKeySet();
        testValues();
        testEntrySet();
    }

    private static SortedMap<String, Integer> initMap() {
        SortedMap<String, Integer> map = new TreeMap<>();
        map.put("A", 1);
        map.put("B", 2);
        map.put("C", 3);
        map.put("D", 4);
        map.put("E", 5);
        return map;
    }

    private static void testComparator() {
        SortedMap<String, Integer> map = initMap();
        assertEquals("SortedMapTest.testComparator", null, map.comparator());
    }

    private static void testSubMap() {
        SortedMap<String, Integer> map = initMap();
        assertEquals("SortedMapTest.testSubMap", "{B=2, C=3, D=4}" , map.subMap("B", "E").toString());
    }

    private static void testHeadMap() {
        SortedMap<String, Integer> map = initMap();
        assertEquals("SortedMapTest.testHeadMap", "{A=1, B=2}" , map.headMap("C").toString());
    }

    private static void testTailMap() {
        SortedMap<String, Integer> map = initMap();
        assertEquals("SortedMapTest.testTailMap", "{C=3, D=4, E=5}" , map.tailMap("C").toString());
    }

    private static void testFirstKey() {
        SortedMap<String, Integer> map = initMap();
        assertEquals("SortedMapTest.testFirstKey", "A" , map.firstKey());
    }

    private static void testLastKey() {
        SortedMap<String, Integer> map = initMap();
        assertEquals("SortedMapTest.testLastKey", "E" , map.lastKey());
    }

    private static void testKeySet() {
        SortedMap<String, Integer> map = initMap();
        assertEquals("SortedMapTest.testKeySet", "[A, B, C, D, E]" , map.keySet().toString());
    }

    private static void testValues() {
        SortedMap<String, Integer> map = initMap();
        assertEquals("SortedMapTest.testValues", "[1, 2, 3, 4, 5]" , map.values().toString());
    }

    private static void testEntrySet() {
        SortedMap<String, Integer> map = initMap();
        assertEquals("SortedMapTest.testEntrySet", "[A=1, B=2, C=3, D=4, E=5]" , map.entrySet().toString());
    }
}
