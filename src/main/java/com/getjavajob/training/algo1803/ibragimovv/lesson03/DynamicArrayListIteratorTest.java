package com.getjavajob.training.algo1803.ibragimovv.lesson03;

import static com.getjavajob.training.util.Assert.assertEquals;
import static com.getjavajob.training.util.Assert.fail;

/**
 * @author vinerI
 */
public class DynamicArrayListIteratorTest {
    private static final String INDEX_OUT_MESSAGE = "ArrayIndexOutOfBoundsException";

    public static void main(String[] args) {
        testListIteratorHasNext();
        testListIteratorNext();
        testListIteratorHasPrevious();
        testListIteratorPrevious();
        testListIteratorNextIndex();
        testListIteratorPreviousIndex();
        testListIteratorRemove();
        testListIteratorSet();
        testListIteratorAdd();
    }

    private static DynamicArray createArray() {
        DynamicArray arrayData = new DynamicArray(5);
        for (int i = 0; i < 5; i++) {
            arrayData.add(i);
        }
        return arrayData;
    }

    private static void testListIteratorHasNext() {
        DynamicArray array = createArray();
        DynamicArray.ListIterator listIterator = array.listIterator();
        listIterator.next();
        listIterator.next();
        listIterator.next();
        listIterator.next();
        listIterator.next();
        try {
            listIterator.next();
            fail("No elements");
        } catch (Exception e) {
            assertEquals("No elements", e.getMessage());
        }

        assertEquals("ListIterator.testListIteratorHasNext", false, listIterator.hasNext());
    }

    private static void testListIteratorNext() {
        DynamicArray array = createArray();
        DynamicArray.ListIterator listIterator = array.listIterator();

        assertEquals("ListIterator.testListIteratorNext", 0, (Integer) listIterator.next());
    }

    private static void testListIteratorHasPrevious() {
        DynamicArray array = createArray();
        DynamicArray.ListIterator listIterator = array.listIterator();
        listIterator.next();
        listIterator.next();

        assertEquals("ListIterator.testListIteratorHasPrevious", true, listIterator.hasPrevious());
    }

    private static void testListIteratorPrevious() {
        DynamicArray array = createArray();
        DynamicArray.ListIterator listIterator = array.listIterator();
        listIterator.next();
        listIterator.next();

        assertEquals("ListIterator.testListIteratorPrevious", 1, (Integer) listIterator.previous());
    }

    private static void testListIteratorNextIndex() {
        DynamicArray array = createArray();
        DynamicArray.ListIterator listIterator = array.listIterator();
        listIterator.next();
        listIterator.next();
        listIterator.next();
        listIterator.next();

        assertEquals("ListIterator.testListIteratorNextIndex", 4, listIterator.nextIndex());
    }

    private static void testListIteratorPreviousIndex() {
        DynamicArray array = createArray();
        DynamicArray.ListIterator listIterator = array.listIterator();
        listIterator.next();
        listIterator.next();
        listIterator.next();
        listIterator.next();

        assertEquals("ListIterator.testListIteratorPreviousIndex", 3, listIterator.previousIndex());
    }

    private static void testListIteratorRemove() {
        DynamicArray array = createArray();
        DynamicArray.ListIterator listIterator = array.listIterator();
        listIterator.next();
        listIterator.next();
        try {
            listIterator.remove();
            fail("Impossible assertion");
        } catch (Exception e) {
            assertEquals( INDEX_OUT_MESSAGE, e.getMessage());
        } catch (AssertionError e) {
            assertEquals("Impossible assertion", e.getMessage());
        }

        assertEquals("ListIterator.testListIteratorRemove", new Integer[]{0, 1, 3, 4}, array.toArray());
    }

    private static void testListIteratorSet() {
        DynamicArray array = createArray();
        DynamicArray.ListIterator listIterator = array.listIterator();
        listIterator.next();
        listIterator.next();
        listIterator.set(13);
        try {
            listIterator.set(13);
            fail("Impossible assertion");
        } catch (Exception e) {
            assertEquals(INDEX_OUT_MESSAGE, e.getMessage());
        } catch (AssertionError e) {
            assertEquals("Impossible assertion", e.getMessage());
        }

        assertEquals("ListIterator.testListIteratorSet", new Integer[]{0, 1, 13, 13, 4}, array.toArray());
    }

    private static void testListIteratorAdd() {
        DynamicArray array = createArray();
        DynamicArray.ListIterator listIterator = array.listIterator();
        listIterator.next();
        listIterator.add(13);
        try {
            listIterator.add(13);
            fail("Impossible assertion");
        } catch (Exception e) {
            assertEquals(INDEX_OUT_MESSAGE, e.getMessage());
        } catch (AssertionError e) {
            assertEquals("Impossible assertion", e.getMessage());
        }

        assertEquals("ListIterator.testListIteratorAdd", new Integer[]{0, 13, 13, 1, 2, 3, 4}, array.toArray());
    }
}
