package com.getjavajob.training.algo1803.ibragimovv.lesson08.binary.search.balanced;

import com.getjavajob.training.algo1803.ibragimovv.lesson07.Node;
import com.getjavajob.training.algo1803.ibragimovv.lesson08.binary.search.BinarySearchTree;

/**
 * @author vinerI
 */
public abstract class BalanceableTree<E> extends BinarySearchTree<E> {

    /**
     * Sets new relationship between parent and child. This method is used by
     * {@link #rotate(com.getjavajob.training.algo1803.ibragimovv.lesson07.Node)} for node and its grandparent,
     * node and its parent, node's child and node's parent relinking.
     *
     * @param parent        new parent
     * @param child         new child
     * @param makeLeftChild whether new child must be left or right
     */
    private void relink(NodeImpl<E> parent, NodeImpl<E> child, boolean makeLeftChild) {
        if (parent == root()) {
            child.setParent(null);
            root = child;
        } else {
            if (parent.getParent().getLeftChild() == parent) {
                parent.getParent().setLeftChild(child);
            } else {
                parent.getParent().setRightChild(child);
            }
            child.setParent(parent.getParent());
        }
        parent.setParent(child);

        if (makeLeftChild) {
            if (child.getRightChild() != null) {
                child.getRightChild().setParent(parent);
            }
            parent.setLeftChild(child.getRightChild());
            child.setRightChild(parent);
        } else {
            if (child.getLeftChild() != null) {
                child.getLeftChild().setParent(parent);
            }
            parent.setRightChild(child.getLeftChild());
            child.setLeftChild(parent);
        }
    }

    /**
     * Rotates n with it's parent.
     *
     * @param n node to rotate above its parent
     */
    protected void rotate(Node<E> n) {
        NodeImpl<E> node = validate(n);
        boolean makeLeftChild = makeLeftChild(node);
        relink(node.getParent(), node, makeLeftChild);
    }

    private boolean makeLeftChild(NodeImpl<E> node) {
        return compare(node.getElement(), node.getParent().getElement()) < 0;
    }

    /**
     * Performs one rotation of <i>n</i>'s parent node or two rotations of <i>n</i> by the means of
     * {@link #rotate(com.getjavajob.training.algo1803.ibragimovv.lesson07.Node)} to reduce the height of subtree rooted at <i>n1</i>
     * <p>
     * <pre>
     *     n1         n2           n1           n
     *    /          /  \         /            / \
     *   n2    ==>  n   n1  or  n2     ==>   n2   n1
     *  /                         \
     * n                           n
     * </pre>
     * <p>
     * Similarly for subtree with right side children.
     *
     * @param n grand child of subtree root node
     * @return new subtree root
     */
    protected Node<E> reduceSubtreeHeight(Node<E> n) {
        NodeImpl<E> node = validate(n);
        NodeImpl<E> parent = node.getParent();

        if (node.getParent() != null && parent.getParent() != null) {
            if (makeLeftChild(node) && makeLeftChild(parent)) {
                relink(parent.getParent(), parent, makeLeftChild(parent));
                return parent;
            } else {
                rotate(node);
                relink(node.getParent(), parent.getParent(), makeLeftChild(parent));
                return node;
            }
        }
        return null;
    }
}
