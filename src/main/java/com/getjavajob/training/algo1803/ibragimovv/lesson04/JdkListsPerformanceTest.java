package com.getjavajob.training.algo1803.ibragimovv.lesson04;

import com.getjavajob.training.util.StopWatch;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author vinerI
 */
public class JdkListsPerformanceTest {
    private static final int NUMBER_OF_ELEMENTS = 100_000;

    public static void main(String[] args) {
        testAddBeginArray();
        testAddBeginLinked();
        testAddMiddleArray();
        testAddMiddleLinked();
        testAddEndArray();
        testAddEndLinked();
        testRemoveBeginArray();
        testRemoveBeginLinked();
        testRemoveMiddleArray();
        testRemoveMiddleLinked();
        testRemoveEndArray();
        testRemoveEndLinked();
    }

    private static void testAddBeginArray() {
        List<Integer> array = new ArrayList<>();
        array.add(10);
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            array.add(0, i);
        }
        System.out.println("-------- Addition to the beginning --------");
        System.out.println("ArrayList.add(e): " + time.getElapsedTime() + " ms");
    }

    private static void testAddBeginLinked() {
        List<Integer> list = new LinkedList<>();
        list.add(10);
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(0, i);
        }
        System.out.println("LinkedList.add(e): " + time.getElapsedTime() + " ms");
        System.out.println("-------- etc -------");
        System.out.println();
    }

    private static void testAddMiddleArray() {
        List<Integer> array = new ArrayList<>();
        array.add(10);
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            array.add(array.size() / 2, i);
        }
        System.out.println("-------- Addition to the middle --------");
        System.out.println("ArrayList.add(e): " + time.getElapsedTime() + " ms");
    }

    private static void testAddMiddleLinked() {
        List<Integer> list = new LinkedList<>();
        list.add(10);
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(list.size() / 2, i);
        }
        System.out.println("LinkedList.add(e): " + time.getElapsedTime() + " ms");
        System.out.println("-------- etc -------");
        System.out.println();
    }

    private static void testAddEndArray() {
        List<Integer> array = new ArrayList<>();
        array.add(10);
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS * 10; i++) {
            array.add(array.size() - 1, i);
        }
        System.out.println("-------- Addition to the end --------");
        System.out.println("ArrayList.add(e): " + time.getElapsedTime() + " ms");
    }

    private static void testAddEndLinked() {
        List<Integer> list = new LinkedList<>();
        list.add(10);
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS * 10; i++) {
            list.add(list.size() - 1, i);
        }
        System.out.println("LinkedList.add(e): " + time.getElapsedTime() + " ms");
        System.out.println("-------- etc -------");
        System.out.println();
    }

    private static void testRemoveBeginArray() {
        List<Integer> array = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            array.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            array.remove(0);
        }
        System.out.println("-------- Remove from the beginning --------");
        System.out.println("ArrayList.add(e): " + time.getElapsedTime() + " ms");
    }

    private static void testRemoveBeginLinked() {
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.remove(0);
        }
        System.out.println("LinkedList.add(e): " + time.getElapsedTime() + " ms");
        System.out.println("-------- etc -------");
        System.out.println();
    }

    private static void testRemoveMiddleArray() {
        List<Integer> array = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            array.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            array.remove(array.size() / 2);
        }
        System.out.println("-------- Remove from the middle --------");
        System.out.println("ArrayList.add(e): " + time.getElapsedTime() + " ms");
    }

    private static void testRemoveMiddleLinked() {
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            list.remove(list.size() / 2);
        }
        System.out.println("LinkedList.add(e): " + time.getElapsedTime() + " ms");
        System.out.println("-------- etc -------");
        System.out.println();
    }

    private static void testRemoveEndArray() {
        List<Integer> array = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS * 10; i++) {
            array.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS * 10; i++) {
            array.remove(array.size() - 1);
        }
        System.out.println("-------- Remove from the end --------");
        System.out.println("ArrayList.add(e): " + time.getElapsedTime() + " ms");
    }

    private static void testRemoveEndLinked() {
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < NUMBER_OF_ELEMENTS * 10; i++) {
            list.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS * 10; i++) {
            list.remove(list.size() - 1);
        }
        System.out.println("LinkedList.add(e): " + time.getElapsedTime() + " ms");
        System.out.println("-------- etc -------");
        System.out.println();
    }
}
