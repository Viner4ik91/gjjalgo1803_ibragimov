package com.getjavajob.training.algo1803.ibragimovv.lesson01;

import static com.getjavajob.training.algo1803.ibragimovv.lesson01.Task07.solveArithmeticSwapOne;
import static com.getjavajob.training.algo1803.ibragimovv.lesson01.Task07.solveArithmeticSwapTwo;
import static com.getjavajob.training.algo1803.ibragimovv.lesson01.Task07.solveBitwiseSwapOne;
import static com.getjavajob.training.algo1803.ibragimovv.lesson01.Task07.solveBitwiseSwapTwo;
import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class Task07Test {
    public static void main(String[] args) {
        testBitwiseOne();
        testBitwiseTwo();
        testArithmeticOne();
        testArithmeticTwo();
    }

    private static void testBitwiseOne() {
        assertEquals("Task07.testBitwiseOne", "3 2", solveBitwiseSwapOne(2, 3));
    }

    private static void testBitwiseTwo() {
        assertEquals("Task07.testBitwiseTwo", "3 2", solveBitwiseSwapTwo(2, 3));
    }

    private static void testArithmeticOne() {
        assertEquals("Task07.testArithmeticOne", "3 2", solveArithmeticSwapOne(2, 3));
    }

    private static void testArithmeticTwo() {
        assertEquals("Task07.testArithmeticTwo", "3 2", solveArithmeticSwapTwo(2, 3));
    }
}
