package com.getjavajob.training.algo1803.ibragimovv.lesson05;

import static com.getjavajob.training.util.Assert.assertEquals;
import static com.getjavajob.training.util.Assert.fail;

/**
 * @author vinerI
 */
public class LinkedListStackTest {
    public static void main(String[] args) {
        testPush();
        testPop();
    }

    private static void testPush() {
        LinkedListStack<Integer> listStack = new LinkedListStack<>();
        for (int i = 0; i < 5; i++) {
            listStack.push(i);
        }
        int[] expected = new int[]{4, 3, 2, 1, 0};
        assertEquals("LinkedListStackTest.testPush", expected, listStack.asListStack().toArray());
    }

    private static void testPop() {
        LinkedListStack<Integer> listStack = new LinkedListStack<>();
        try {
            listStack.pop();
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("Empty list", e.getMessage());
        }

        for (int i = 0; i < 5; i++) {
            listStack.push(i);
        }
        listStack.pop();
        listStack.pop();
        int[] expected = new int[]{2, 1, 0};
        assertEquals("LinkedListStackTest.testPop", expected, listStack.asListStack().toArray());
    }
}
