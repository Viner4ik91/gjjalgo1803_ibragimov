package com.getjavajob.training.algo1803.ibragimovv.lesson01;

import static com.getjavajob.training.algo1803.ibragimovv.lesson01.Task06.solveA;
import static com.getjavajob.training.algo1803.ibragimovv.lesson01.Task06.solveB;
import static com.getjavajob.training.algo1803.ibragimovv.lesson01.Task06.solveC;
import static com.getjavajob.training.algo1803.ibragimovv.lesson01.Task06.solveD;
import static com.getjavajob.training.algo1803.ibragimovv.lesson01.Task06.solveE;
import static com.getjavajob.training.algo1803.ibragimovv.lesson01.Task06.solveF;
import static com.getjavajob.training.algo1803.ibragimovv.lesson01.Task06.solveG;
import static com.getjavajob.training.algo1803.ibragimovv.lesson01.Task06.solveH;
import static com.getjavajob.training.algo1803.ibragimovv.lesson01.Task06.solveI;
import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class Task06Test {
    public static void main(String[] args) {
        testSolveA();
        testSolveB();
        testSolveC();
        testSolveD();
        testSolveE();
        testSolveF();
        testSolveG();
        testSolveH();
        testSolveI();
    }

    private static void testSolveA() {
        assertEquals("Task06.testSolveA", 0b100, solveA(0b10));
    }

    private static void testSolveB() {
        assertEquals("Task06.testSolveB", 0b1000, solveB(0b10, 0b10));
    }

    private static void testSolveC() {
        assertEquals("Task06.testSolveC", 0b10000, solveC(0b11101, 0b100));
    }

    private static void testSolveD() {
        assertEquals("Task06.testSolveD", 0b1101, solveD(0b1001, 0b10));
    }

    private static void testSolveE() {
        assertEquals("Task06.testSolveE", 0b11111, solveE(0b1111, 0b100));
    }

    private static void testSolveF() {
        assertEquals("Task06.testSolveF", 0b1011, solveF(0b1111, 0b10));
    }

    private static void testSolveG() {
        assertEquals("Task06.testSolveG", 0b1111, solveG(0b11111, 0b100));
    }

    private static void testSolveH() {
        assertEquals("Task06.testSolveH", 0b1111, solveH(0b11111, 0b100));
    }

    private static void testSolveI() {
        assertEquals("Task06.testSolveI", "0B101", solveI(0b101));
    }
}
