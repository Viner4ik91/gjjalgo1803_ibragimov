package com.getjavajob.training.algo1803.ibragimovv.lesson06;

/**
 * @author vinerI
 */
public interface Matrix<V> {
    V get(int i, int j);

    V set(int i, int j, V value);
}
