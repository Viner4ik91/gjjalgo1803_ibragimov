package com.getjavajob.training.algo1803.ibragimovv.lesson07;

/**
 * An abstraction for a node of a tree
 *
 * @param <E> element
 */
public interface Node<E> {

    /**
     *
     * @return the element stored at this node
     */
    E getElement();

}
