package com.getjavajob.training.algo1803.ibragimovv.lesson05;

import java.util.ArrayDeque;
import java.util.Queue;

import static com.getjavajob.training.util.Assert.assertEquals;
import static com.getjavajob.training.util.Assert.fail;

/**
 * @author vinerI
 */
public class QueueTest {
    public static void main(String[] args) {
        testAdd();
        testOffer();
        testRemove();
        testPoll();
        testElement();
        testPeek();
    }

    private static void testAdd() {
        Queue<Integer> listQueue = new ArrayDeque<>();
        listQueue.add(1);
        listQueue.add(2);
        int[] expected = new int[] {1, 2};
        assertEquals("QueueTest.testAdd", expected, listQueue.toArray());
    }

    private static void testOffer() {
        Queue<Integer> listQueue = new ArrayDeque<>();
        listQueue.add(1);
        listQueue.add(2);
        int[] expected = new int[] {1, 2, 3};
        listQueue.offer(3);
        assertEquals("QueueTest.testOffer", expected, listQueue.toArray());
    }

    private static void testRemove() {
        Queue<Integer> listQueue = new ArrayDeque<>();
        try {
            listQueue.remove();
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("NoSuchElementException", e.getClass().getSimpleName());
        }

        for (int i = 0; i < 3; i++) {
            listQueue.add(i);
        }
        listQueue.remove();
        listQueue.remove();
        int[] expected = new int[]{2};
        assertEquals("QueueTest.testRemove", expected, listQueue.toArray());
    }

    private static void testPoll() {
        Queue<Integer> listQueue = new ArrayDeque<>();
        try {
            int removed = listQueue.poll();
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("NullPointerException", e.getClass().getSimpleName());
        }

        for (int i = 0; i < 3; i++) {
            listQueue.add(i);
        }
        listQueue.poll();
        listQueue.poll();
        int[] expected = new int[]{2};
        assertEquals("QueueTest.testPoll", expected, listQueue.toArray());
    }

    private static void testElement() {
        Queue<Integer> listQueue = new ArrayDeque<>();
        try {
            listQueue.element();
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("NoSuchElementException", e.getClass().getSimpleName());
        }
        listQueue.add(1);
        listQueue.add(2);
        assertEquals("QueueTest.testElement", 1, listQueue.element());
    }

    private static void testPeek() {
        Queue<Integer> listQueue = new ArrayDeque<>();
        listQueue.add(1);
        listQueue.add(2);
        assertEquals("QueueTest.testPeek", 1, listQueue.peek());
    }
}
