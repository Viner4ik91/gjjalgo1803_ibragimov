package com.getjavajob.training.algo1803.ibragimovv.lesson10;

import java.util.Arrays;
import java.util.Random;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class MergeSortTest {
    public static void main(String[] args) {
        testSort();
        testSortMaxValue();
    }

    private static void testSort() {
        int[] initArray = new int[]{3, 7, 4, 9, 5, 2, 6, 1};
        int[] expected = new int[]{1, 2, 3, 4, 5, 6, 7, 9};
        MergeSort ms = new MergeSort();
        ms.sort(initArray);
        assertEquals("MergeSortTest.testSort()", Arrays.toString(expected), Arrays.toString(initArray));
    }

    private static void testSortMaxValue() {
        byte[] maxArray = new byte[Byte.MAX_VALUE]; //memory of my notebook is 4 Gb, i need 8 to pass max Integer
        Random random = new Random();
        for (int i = 0; i < Byte.MAX_VALUE; i++) {
            maxArray[i] = (byte)random.nextInt();
        }
        MergeSort ms = new MergeSort();
        byte[] expected = maxArray;
        ms.sort(expected);
        ms.sort(maxArray);
        assertEquals("MergeSortTest.testMaxValue()", expected, maxArray);
    }
}
