package com.getjavajob.training.algo1803.ibragimovv.lesson08.binary.search.balanced;

import com.getjavajob.training.algo1803.ibragimovv.lesson07.Node;

/**
 * @author vinerI
 */
public class BalanceableTreeForTest<E> extends BalanceableTree<E> {
    @Override
    protected void rotate(Node<E> n) {
        super.rotate(n);
    }

    @Override
    protected Node<E> reduceSubtreeHeight(Node<E> n) {
        return super.reduceSubtreeHeight(n);
    }
}
