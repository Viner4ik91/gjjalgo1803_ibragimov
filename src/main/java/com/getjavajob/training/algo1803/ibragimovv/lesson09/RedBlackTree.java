package com.getjavajob.training.algo1803.ibragimovv.lesson09;

import com.getjavajob.training.algo1803.ibragimovv.lesson07.Node;
import com.getjavajob.training.algo1803.ibragimovv.lesson08.binary.search.balanced.BalanceableTree;

/**
 * @author vinerI
 */
public class RedBlackTree<E> extends BalanceableTree<E> {

    @Override
    protected NodeRB<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n instanceof NodeRB) {
            return (NodeRB<E>) n;
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    protected NodeImpl<E> createNode(E e) {
        return new NodeRB<>(e);
    }

    private boolean isBlack(Node<E> n) {
        return validate(n).isBlack();
    }

    private boolean isRed(Node<E> n) {
        return validate(n).isRed();
    }

    private void makeBlack(Node<E> n) {
        validate(n).setBlack();
    }

    private void makeRed(Node<E> n) {
        validate(n).setRed();
    }

    private NodeRB<E> getGrandparent(Node<E> n) {
        NodeRB<E> node = validate(n);
        return node != null && node.getParent().getParent() != null ? validate(node.getParent().getParent()) : null;
    }

    private NodeRB<E> getUncle(Node<E> n) {
        NodeRB<E> node = validate(n);
        NodeRB<E> grandparent = getGrandparent(n);
        if (grandparent == null) {
            return null;
        }
        if (sibling(node.getParent()) != null) {
            return validate(sibling(node.getParent()));
        } else {
            return null;
        }
    }

    @Override
    protected void afterElementAdded(Node<E> n) {
        NodeRB<E> node = validate(n);
        if (node.getParent() == null) {
            node.setBlack();
        } else {
            NodeRB<E> grandparent = getGrandparent(n);
            NodeRB<E> parent = validate(node.getParent());
            NodeRB<E> uncle = getUncle(n);

            if (n != null && isRed(parent)) {
                if (uncle != null && isRed(uncle)) {
                    makeBlack(parent);
                    makeBlack(uncle);
                    makeRed(grandparent);
                    afterElementAdded(grandparent);
                } else if (parent == left(grandparent)) {
                    if (node == parent.getRightChild()) {
                        rotate(node);
                        node = validate(node.getLeftChild());
                    }
                    makeBlack(node.getParent());
                    makeRed(grandparent);
                    rotate(node.getParent());
                } else if (parent == right(grandparent)) {
                    if (node == parent.getLeftChild()) {
                        rotate(node);
                        node = validate(node.getRightChild());
                    }
                    makeBlack(node.getParent());
                    makeRed(grandparent);
                    rotate(node.getParent());
                }
            }
        }
    }

    @Override
    protected void afterElementRemoved(Node<E> n) {
        NodeRB<E> node = validate(n);
        while (node != root && isBlack(node)) {
            if (node == left(parent(node))) { //node is a left child
                NodeRB<E> brother = validate(sibling(node));
                if (isRed(brother)) {
                    makeBlack(brother);
                    makeRed(parent(node));
                    rotate(parent(node));
                    brother = validate(sibling(node));
                }
                if (isBlack(left(brother)) && isBlack(right(brother))) {
                    makeRed(brother);
                    node = validate(parent(node));
                } else {
                    if (isBlack(right(brother))) {
                        makeBlack(left(brother));
                        makeRed(brother);
                        rotate(right(brother));
                        brother = validate(right(parent(node)));
                    }
                    if (isBlack(validate(parent(node)))) {
                        makeBlack(brother);
                    } else {
                        makeRed(brother);
                    }
                    makeBlack(parent(node));
                    makeBlack(right(brother));
                    rotate(node);
                    node = validate(root);
                }
            } else { //node is a right child
                NodeRB<E> brother = validate(sibling(node));
                if (isRed(brother)) {
                    makeBlack(brother);
                    makeRed(parent(node));
                    rotate(node);
                    brother = validate(sibling(node));
                }
                if (isBlack(left(brother)) && isBlack(right(brother))) {
                    makeRed(brother);
                    node = validate(parent(node));
                } else {
                    if (isBlack(left(brother))) {
                        makeBlack(left(brother));
                        makeRed(brother);
                        rotate(left(brother));
                        brother = validate(left(parent(node)));
                    }
                    if (isBlack(parent(node))) {
                        makeBlack(brother);
                    } else {
                        makeRed(brother);
                    }
                    makeBlack(parent(node));
                    makeBlack(left(brother));
                    rotate(node);
                    node = validate(root);
                }
            }
        }
        makeBlack(node);
    }

    private static class NodeRB<E> extends NodeImpl<E> {
        private boolean redColor;

        private NodeRB(E val) {
            super(val);
            this.redColor = true;
        }

        private boolean isRed() {
            return redColor;
        }

        private boolean isBlack() {
            return !redColor;
        }

        private void setRed() {
            this.redColor = true;
        }

        private void setBlack() {
            this.redColor = false;
        }
    }
}
