package com.getjavajob.training.algo1803.ibragimovv.lesson05;

import java.util.Deque;
import java.util.LinkedList;

import static java.lang.Character.isDigit;
import static java.lang.Double.parseDouble;

/**
 * @author vinerI
 */
public class ExpressionCalculator {

    private static boolean isOperator(char i) {
        return getPriority(i) > 0;
    }

    private static int getPriority(char i) {
        if (i == '-' || i == '+') {
            return 1;
        } else if (i == '*' || i == '/') {
            return 2;
        } else if (i == '(' || i == ')') {
            return -1;
        } else {
            return 0;
        }
    }

    public String convertExpression(String infixExpression) {
        StringBuilder postfixExpression = new StringBuilder();
        Deque<Character> operator = new LinkedList<>();

        for (int i = 0; i < infixExpression.length(); i++) {
            char token = infixExpression.charAt(i);
            if (isDigit(token)) {
                postfixExpression.append(token).append(' ');
            } else if (token == '(') {
                operator.push(token);
            } else if (token == ')') {
                try {
                    while (operator.peek() != '(') {
                        postfixExpression.append(operator.pop()).append(' ');
                    }
                    operator.pop();
                } catch (NullPointerException e) {
                    System.out.println("Incorrect number of brackets, calculation without the last bracket");
                }
            } else if (isOperator(token)) {
                while (!operator.isEmpty() && getPriority(operator.peek()) >= getPriority(token)) {
                    postfixExpression.append(operator.pop()).append(' ');
                }
                operator.push(token);
            }
        } while (!operator.isEmpty()) {
            if (postfixExpression.length() > 0 && postfixExpression.charAt(postfixExpression.length() - 1) != ' ') {
                postfixExpression.append(' ');
            }
            postfixExpression.append(operator.pop());
        }
        return postfixExpression.toString();
    }

    private double calculationLogic(String operation, double digit1, double digit2) {
        switch (operation) {
            case "+":
                return digit2 + digit1;
            case "-":
                return digit2 - digit1;
            case "*":
                return digit2 * digit1;
            case "/":
                return digit2 / digit1;
            default:
                return 0;
        }
    }

    public double calculateExpression(String postfix) {
        Deque<Double> stack = new LinkedList<>();
        for (String token : postfix.split(" ")) {
            if (isOperator(token.charAt(0))) {
                stack.push(calculationLogic(token, stack.pop(), stack.pop()));
            } else if (isDigit(token.charAt(0))) {
                stack.push(parseDouble(token));
            }
        }
        return stack.pop();
    }
}
