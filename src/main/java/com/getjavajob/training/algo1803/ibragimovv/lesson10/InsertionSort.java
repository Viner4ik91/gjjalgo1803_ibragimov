package com.getjavajob.training.algo1803.ibragimovv.lesson10;

/**
 * @author vinerI
 */
public class InsertionSort {
    public void sort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int x = array[i];
            int j = i - 1;
            while (j >= 0 && array[j] > x) {
                array[j + 1] = array[j];
                j = j - 1;
            }
            array[j + 1] = x;
        }
    }
}
