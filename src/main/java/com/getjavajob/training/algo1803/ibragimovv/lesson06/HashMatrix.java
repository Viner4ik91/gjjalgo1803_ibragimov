package com.getjavajob.training.algo1803.ibragimovv.lesson06;

import java.util.HashMap;

/**
 * @author vinerI
 */
public class HashMatrix<V> implements Matrix<V> {
    private HashMap<MapKey, V> matrix = new HashMap<>();

    @Override
    public V get(int i, int j) {
        return matrix.get(new MapKey(i, j));
    }

    @Override
    public V set(int i, int j, V value) {
        return matrix.put(new MapKey(i, j), value);
    }
}
