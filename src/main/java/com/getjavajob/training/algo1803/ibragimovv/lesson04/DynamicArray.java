package com.getjavajob.training.algo1803.ibragimovv.lesson04;

import java.util.ConcurrentModificationException;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import static java.lang.System.arraycopy;

/**
 * @author vinerI
 */
public class DynamicArray<E> extends AbstractList<E> {
    private E[] elementData;
    private int size;
    private int modCount;

    @SuppressWarnings("unchecked")
    public DynamicArray(int specifiedSize) {
        if (specifiedSize > 0) {
            this.elementData = (E[]) new Object[specifiedSize];
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " + specifiedSize);
        }
    }

    @SuppressWarnings("unchecked")
    public DynamicArray() {
        this.elementData = (E[]) new Object[10];
    }

    private void checkIndexOutOfBounds(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("ArrayIndexOutOfBoundsException");
        }
    }

    @SuppressWarnings("unchecked")
    private void checkSize() {
        if (size == elementData.length) {
            E[] growArray = (E[]) new Object[(int) (elementData.length * 1.5)];
            arraycopy(elementData, 0, growArray, 0, elementData.length);
            elementData = growArray;
        }
    }

    @Override
    public boolean add(E element) {
        checkSize();
        elementData[size] = element;
        size++;
        modCount++;
        return true;
    }

    @Override
    public void add(int index, E element) {
        checkIndexOutOfBounds(index);
        checkSize();
        if (index < size - 1) {
            arraycopy(elementData, index, elementData, index + 1, size - index);
        }
        elementData[index] = element;
        modCount++;
        size++;
    }

    @Override
    public E set(int index, E element) {
        checkIndexOutOfBounds(index);
        E oldValue = elementData[index];
        elementData[index] = element;
        return oldValue;
    }

    @Override
    public E get(int index) {
        checkIndexOutOfBounds(index);
        return elementData[index];
    }

    @Override
    public E remove(int index) {
        checkIndexOutOfBounds(index);
        modCount++;
        E removedElement = elementData[index];
        if (index <= size - 1) {
            arraycopy(elementData, index + 1, elementData, index, size - index - 1);
        }
        if (index == size) {
            elementData[index] = null;
        }
        size--;
        return removedElement;
    }

    @Override
    public boolean remove(Object element) {
        if (element == null) {
            for (int index = 0; index < size; index++)
                if (elementData[index] == null) {
                    remove(index);
                    return true;
                }
        } else {
            for (int index = 0; index < size; index++)
                if (element.equals(elementData[index])) {
                    remove(index);
                    return true;
                }
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int indexOf(Object element) {
        if (element == null) {
            for (int i = 0; i < size; i++) {
                if (elementData[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (element.equals(elementData[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public boolean contains (Object element) {
        return indexOf(element) >= 0;
    }

    @Override
    public Object[] toArray () {
        Object[] array = new Object[size];
        arraycopy(elementData, 0, array, 0, size);
        return array;
    }

    public ListIteratorImpl listIterator() {
        return new ListIteratorImpl();
    }

    public class ListIteratorImpl implements ListIterator<E> {
        int position = 0;
        int modCount;

        private ListIteratorImpl() {
            this.modCount = DynamicArray.this.modCount;
        }

        @Override
        public boolean hasNext() {
            return position != size;
        }

        @Override
        public E next(){
            checkConcurrentModificationException();
            if (position >= size) {
                throw new NoSuchElementException("No elements");
            }
            return get(position++);
        }

        @Override
        public boolean hasPrevious(){
            return position > 0;
        }

        @Override
        public E previous(){
            if (position <= 0) {
                throw new NoSuchElementException("No elements");
            }
            return get(--position);
        }

        @Override
        public int nextIndex(){
            return position;
        }

        @Override
        public int previousIndex(){
            return position - 1;
        }

        @Override
        public void remove(){
            checkConcurrentModificationException();
            DynamicArray.this.remove(position);
            modCount = DynamicArray.this.modCount;
        }

        @Override
        public void set(E e){
            checkConcurrentModificationException();
            DynamicArray.this.set(position++, e);
        }

        @Override
        public void add(E e){
            checkConcurrentModificationException();
            DynamicArray.this.add(position++, e);
            modCount = DynamicArray.this.modCount;
        }

        private void checkConcurrentModificationException() {
            if (modCount != DynamicArray.this.modCount) {
                throw new ConcurrentModificationException("Error of modification the object");
            }
        }

    }
}