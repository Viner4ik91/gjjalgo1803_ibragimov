package com.getjavajob.training.algo1803.ibragimovv.lesson05;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

interface Predicate<E> {
    boolean checkElement(E e);
}

interface Transformer<I, O> {
    O transformObject(I input);
}

interface CollectionLogic<E> {
    E executionLogic(E element);
}

/**
 * @author vinerI
 */
public class CollectionUtils<E> {
    public static <E> boolean filter(Iterable<E> collection, Predicate<E> predicate) {
        boolean modification = false;
        Iterator<E> collIt = collection.iterator();
        while (collIt.hasNext()) {
            if (!predicate.checkElement(collIt.next())) {
                collIt.remove();
                modification = true;
            }
        }
        return modification;
    }

    public static <I, O> Collection<O> transform(Collection<I> collection, Transformer<I, O> transformer) {
        List<O> transformedList = new ArrayList<>();
        for (I object : collection) {
            transformedList.add(transformer.transformObject(object));
        }
        return transformedList;
    }

    public static <I, O> Collection<O> transformMod(Collection<I> collection, Transformer<I, O> transformer) {
        Collection<O> virtualCollection = transform(collection, transformer);
        collection.clear();
        collection.addAll((Collection<I>) virtualCollection);
        return (Collection<O>) collection;
    }

    public static <E> List<E> forAllDo(List<E> list, CollectionLogic<E> logic) {
        for (E elements : list) {
            logic.executionLogic(elements);
        }
        return list;
    }

    public static <E> Collection<E> unmodifiableCollection(Collection<E> collection) {
        return new UnmodifiableCollection<>(collection);
    }

    public static class UnmodifiableCollection<E> implements Collection<E> {
        Collection<E> coll;

        UnmodifiableCollection(Collection<E> coll) {
            if (coll != null) {
                this.coll = coll;
            } else {
                throw new NullPointerException();
            }
        }

        @Override
        public int size() {
            return coll.size();
        }

        @Override
        public boolean isEmpty() {
            return coll.isEmpty();
        }

        @Override
        public boolean contains(Object o) {
            return coll.contains(o);
        }

        @Override
        public Iterator<E> iterator() {
            return new Iterator<E>() {
                private final Iterator<? extends E> iterator = coll.iterator();

                @Override
                public boolean hasNext() {
                    return iterator.hasNext();
                }

                @Override
                public E next() {
                    return iterator.next();
                }

                @Override
                public void remove() {
                    throw new UnsupportedOperationException("remove");
                }
            };
        }

        @Override
        public Object[] toArray() {
            return coll.toArray();
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return coll.toArray(a);
        }

        @Override
        public boolean add(E e) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean remove(Object o) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean addAll(Collection<? extends E> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void clear() {
            throw new UnsupportedOperationException();
        }
    }
}
