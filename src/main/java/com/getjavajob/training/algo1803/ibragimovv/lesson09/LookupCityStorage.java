package com.getjavajob.training.algo1803.ibragimovv.lesson09;

import java.util.NavigableSet;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author vinerI
 */
public class LookupCityStorage {
    private NavigableSet<String> storage = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);

    public void addCity(String name) {
        storage.add(name);
    }

    public String lookup(String substring) {
        SortedSet<String> set = storage.subSet(substring, true, substring + Character.MAX_VALUE, true);
        StringBuilder result = new StringBuilder();
        for (String name : set) {
            result.append(name).append(" ");
        }
        return result.toString();
    }
}
