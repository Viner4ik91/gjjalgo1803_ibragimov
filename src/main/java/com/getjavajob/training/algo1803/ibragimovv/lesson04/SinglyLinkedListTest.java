package com.getjavajob.training.algo1803.ibragimovv.lesson04;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class SinglyLinkedListTest {
    public static void main(String[] args) {
        testAdd();
        testGet();
        testSize();
        testReverse();
        testRelink();
        testAsList();
    }

    private static void testAdd() {
        SinglyLinkedList<Integer> list = new SinglyLinkedList<>();
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
        int[] expected = new int[]{0, 1, 2, 3, 4};
        assertEquals("SinglyLinkedListTest.testAdd", expected, list.asList().toArray());
    }

    private static void testGet() {
        SinglyLinkedList<Integer> list = new SinglyLinkedList<>();
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
        list.get(3);
        assertEquals("SinglyLinkedListTest.testGet", 3, list.get(3));
    }

    private static void testSize() {
        SinglyLinkedList<Integer> list = new SinglyLinkedList<>();
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
        assertEquals("SinglyLinkedListTest.testSize", 5, list.size());
    }

    private static void testReverse() {
        SinglyLinkedList<Integer> list = new SinglyLinkedList<>();
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
        list.reverse();
        int[] expected = new int[]{4, 3, 2, 1, 0};
        assertEquals("SinglyLinkedListTest.testReverse", expected, list.asList().toArray());
    }

    private static void testRelink() {
        SinglyLinkedList<Integer> list = new SinglyLinkedList<>();
        for (int i = 0; i < 2; i++) {
            list.add(i);
        }
        list.relink(0, 1);
        int[] expected = new int[]{1, 0};
        assertEquals("SinglyLinkedListTest.testRelink", expected, list.asList().toArray());
    }

    private static void testAsList() {
        SinglyLinkedList<Integer> list = new SinglyLinkedList<>();
        for (int i = 0; i < 5; i++) {
            list.add(i);
        }
        int[] expected = new int[]{0, 1, 2 ,3 ,4};
        assertEquals("SinglyLinkedListTest.testAsList", expected, list.asList().toArray());
    }
}
