package com.getjavajob.training.algo1803.ibragimovv.lesson09;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class LookupCityStorageTest {
    public static void main(String[] args) {
        testLookup();
    }

    private static void testLookup() {
        LookupCityStorage database = new LookupCityStorage();
        database.addCity("Moscow");
        database.addCity("Mogilev");
        database.addCity("Mahachkala");
        database.addCity("Kazan");
        database.addCity("Ufa");
        database.addCity("Kirov");
        assertEquals("LookupCityStorageTest", "Mogilev Moscow ", database.lookup("mo" ));
    }
}
