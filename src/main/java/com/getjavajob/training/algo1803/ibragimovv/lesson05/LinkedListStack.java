package com.getjavajob.training.algo1803.ibragimovv.lesson05;

import java.util.List;

/**
 * @author vinerI
 */
public class LinkedListStack<E> implements Stack<E> {

    private SinglyLinkedList<E> listStack = new SinglyLinkedList<>();

    @Override
    public void push(E e) {
        listStack.addToTheTop(e);
    }

    @Override
    public E pop() {
        return listStack.removeFromTheTop();
    }

    public List<E> asListStack() {
        return listStack.asList();
    }
}
