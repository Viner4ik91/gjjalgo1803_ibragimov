package com.getjavajob.training.algo1803.ibragimovv.lesson05;

import java.util.ArrayDeque;
import java.util.Deque;

import static com.getjavajob.training.util.Assert.assertEquals;
import static com.getjavajob.training.util.Assert.fail;

/**
 * @author vinerI
 */
public class DequeTest {
    public static void main(String[] args) {
        testAddFirst();
        testAddLast();
        testOfferFirst();
        testOfferLast();
        testRemoveFirst();
        testRemoveLast();
        testPollFirst();
        testPollLast();
        testGetFirst();
        testGetLast();
        testPeekFirst();
        testPeekLast();
        testRemoveFirstOccurrence();
        testRemoveLastOccurrence();
    }

    private static void testAddFirst() {
        Deque<Integer> dequeList = new ArrayDeque<>();
        try {
            dequeList.addFirst(null);
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("NullPointerException", e.getClass().getSimpleName());
        }
        dequeList.add(0);
        dequeList.addFirst(3);
        dequeList.addFirst(2);
        dequeList.addFirst(1);
        Object[] expected = new Object[]{1, 2, 3, 0};
        assertEquals("DequeTest.testAddFirst", expected, dequeList.toArray());
    }

    private static void testAddLast() {
        Deque<Integer> dequeList = new ArrayDeque<>();
        try {
            dequeList.addLast(null);
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("NullPointerException", e.getClass().getSimpleName());
        }
        dequeList.add(0);
        dequeList.addLast(3);
        dequeList.addLast(2);
        dequeList.addLast(1);
        Object[] expected = new Object[]{0, 3, 2, 1};
        assertEquals("DequeTest.testAddLast", expected, dequeList.toArray());
    }

    private static void testOfferFirst() {
        Deque<Integer> dequeList = new ArrayDeque<>();
        try {
            dequeList.offerFirst(null);
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("NullPointerException", e.getClass().getSimpleName());
        }
        dequeList.add(0);
        dequeList.offerFirst(3);
        dequeList.offerFirst(2);
        dequeList.offerFirst(1);
        Object[] expected = new Object[]{1, 2, 3, 0};
        assertEquals("DequeTest.testOfferFirst", expected, dequeList.toArray());
    }

    private static void testOfferLast() {
        Deque<Integer> dequeList = new ArrayDeque<>();
        try {
            dequeList.offerLast(null);
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("NullPointerException", e.getClass().getSimpleName());
        }
        dequeList.add(0);
        dequeList.offerLast(3);
        dequeList.offerLast(2);
        dequeList.offerLast(1);
        Object[] expected = new Object[]{0, 3, 2, 1};
        assertEquals("DequeTest.testOfferLast", expected, dequeList.toArray());
    }

    private static void testRemoveFirst() {
        Deque<Integer> dequeList = new ArrayDeque<>();
        try {
            dequeList.removeFirst();
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("NoSuchElementException", e.getClass().getSimpleName());
        }
        dequeList.add(1);
        dequeList.add(2);
        dequeList.add(3);
        dequeList.removeFirst();
        Object[] expected = new Object[]{2, 3};
        assertEquals("DequeTest.testRemoveFirst", expected, dequeList.toArray());
    }

    private static void testRemoveLast() {
        Deque<Integer> dequeList = new ArrayDeque<>();
        try {
            dequeList.removeLast();
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("NoSuchElementException", e.getClass().getSimpleName());
        }
        dequeList.add(1);
        dequeList.add(2);
        dequeList.add(3);
        dequeList.removeLast();
        Object[] expected = new Object[]{1, 2};
        assertEquals("DequeTest.testRemoveLast", expected, dequeList.toArray());
    }

    private static void testPollFirst() {
        Deque<Integer> dequeList = new ArrayDeque<>();
        try {
            int removed = dequeList.pollFirst();
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("NullPointerException", e.getClass().getSimpleName());
        }
        dequeList.add(1);
        dequeList.add(2);
        dequeList.add(3);
        assertEquals("DequeTest.testPollFirst", 1, dequeList.pollFirst());
    }

    private static void testPollLast() {
        Deque<Integer> dequeList = new ArrayDeque<>();
        try {
            int removed = dequeList.pollLast();
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("NullPointerException", e.getClass().getSimpleName());
        }
        dequeList.add(1);
        dequeList.add(2);
        dequeList.add(3);
        assertEquals("DequeTest.testPollLast", 3, dequeList.pollLast());
    }

    private static void testGetFirst() {
        Deque<Integer> dequeList = new ArrayDeque<>();
        try {
            dequeList.getFirst();
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("NoSuchElementException", e.getClass().getSimpleName());
        }
        dequeList.add(1);
        dequeList.add(2);
        dequeList.add(3);
        assertEquals("DequeTest.testGetFirst", 1, dequeList.getFirst());
    }

    private static void testGetLast() {
        Deque<Integer> dequeList = new ArrayDeque<>();
        try {
            dequeList.getLast();
            fail("Test failed");
        } catch (AssertionError e) {
            assertEquals("Test failed", e.getMessage());
        } catch (Exception e) {
            assertEquals("NoSuchElementException", e.getClass().getSimpleName());
        }
        dequeList.add(1);
        dequeList.add(2);
        dequeList.add(3);
        assertEquals("DequeTest.testGetLast", 3, dequeList.getLast());
    }

    private static void testPeekFirst() {
        Deque<Integer> dequeList = new ArrayDeque<>();
        dequeList.add(1);
        dequeList.add(2);
        dequeList.add(3);
        assertEquals("DequeTest.testPeekFirst", 1, dequeList.peekFirst());
    }

    private static void testPeekLast() {
        Deque<Integer> dequeList = new ArrayDeque<>();
        dequeList.add(1);
        dequeList.add(2);
        dequeList.add(3);
        assertEquals("DequeTest.testPeekLast", 3, dequeList.peekLast());
    }

    private static void testRemoveFirstOccurrence() {
        Deque<Integer> dequeList = new ArrayDeque<>();
        dequeList.add(1);
        dequeList.add(2);
        dequeList.add(3);
        dequeList.add(2);
        dequeList.add(2);
        dequeList.removeFirstOccurrence(2);
        Object[] expected = new Object[]{1, 3, 2, 2};
        assertEquals("DequeTest.testRemoveFirstOccurrence", expected, dequeList.toArray());
    }

    private static void testRemoveLastOccurrence() {
        Deque<Integer> dequeList = new ArrayDeque<>();
        dequeList.add(1);
        dequeList.add(2);
        dequeList.add(3);
        dequeList.add(2);
        dequeList.add(4);
        dequeList.add(2);
        dequeList.removeLastOccurrence(2);
        Object[] expected = new Object[]{1, 2, 3, 2, 4};
        assertEquals("DequeTest.testRemoveLastOccurrence", expected, dequeList.toArray());
    }
}
