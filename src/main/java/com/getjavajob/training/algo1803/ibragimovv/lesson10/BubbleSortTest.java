package com.getjavajob.training.algo1803.ibragimovv.lesson10;

import java.util.Arrays;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class BubbleSortTest {
    public static void main(String[] args) {
        testSort();
    }

    private static void testSort() {
        int[] initArray = new int[]{5, 1, 4, 2, 8};
        int[] expected = new int[]{1, 2, 4, 5, 8};
        BubbleSort bs = new BubbleSort();
        bs.sort(initArray);
        assertEquals("BubbleSortTest.testSort()", Arrays.toString(expected),  Arrays.toString(initArray));
    }
}
