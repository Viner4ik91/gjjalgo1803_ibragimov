package com.getjavajob.training.algo1803.ibragimovv.lesson04;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class CollectionTest {
    public static void main(String[] args) {
        testSize();
        testIsEmpty();
        testContains();
        testToArray();
        testToArrayObject();
        testAdd();
        testRemove();
        testContainsAll();
        testAddAll();
        testRemoveAll();
        testRetainAll();
        testClear();
        testEquals();
        testHashCode();
    }

    private static void testSize() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(5);
        assertEquals("CollectionTest.testSize", 2, list.size());
    }

    private static void testIsEmpty() {
        List<Integer> list = new ArrayList<>();
        assertEquals("CollectionTest.testIsEmpty", true, list.isEmpty());
    }

    private static void testContains() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(42);
        list.add(3);
        assertEquals("CollectionTest.testContains", true, list.contains(42));
    }

    private static void testToArray() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        int[] expected = new int[]{1, 2, 3};
        assertEquals("CollectionTest.testToArray", expected, list.toArray());
    }

    private static void testToArrayObject() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        Integer objectList[] = new Integer[list.size()];
        int[] expected = {1, 2, 3};
        assertEquals("CollectionTest.testToArrayObject", expected, list.toArray(objectList));
    }

    private static void testAdd() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        int[] expected = new int[]{1, 2};
        assertEquals("CollectionTest.testAdd", expected, list.toArray());
    }

    private static void testRemove() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.remove(1);
        int[] expected = new int[]{1, 3};
        assertEquals("CollectionTest.testRemove", expected, list.toArray());
    }

    private static void testContainsAll() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        List<Integer> array = new ArrayList<>();
        array.add(0);
        array.add(1);
        array.add(2);
        array.add(3);
        assertEquals("CollectionTest.testContainsAll", true, array.containsAll(list));
    }

    private static void testAddAll() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        List<Integer> array = new ArrayList<>();
        array.add(0);
        array.add(1);
        array.add(2);
        list.addAll(array);
        int[] expected = new int[]{1, 2, 0, 1, 2};
        assertEquals("CollectionTest.testAddAll", expected, list.toArray());
    }

    private static void testRemoveAll() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        List<Integer> array = new ArrayList<>();
        array.add(0);
        array.add(1);
        array.add(2);
        array.add(3);
        array.removeAll(list);
        int[] expected = new int[]{0, 3};
        assertEquals("CollectionTest.testRemoveAll", expected, array.toArray());
    }

    private static void testRetainAll() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        List<Integer> array = new ArrayList<>();
        array.add(0);
        array.add(1);
        array.add(2);
        array.add(3);
        array.retainAll(list);
        int[] expected = new int[]{1, 2};
        assertEquals("CollectionTest.testRetainAll", expected, array.toArray());
    }

    private static void testClear() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.clear();
        assertEquals("CollectionTest.testClear", 0, list.size());
    }

    private static void testEquals() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        List<Integer> array = new ArrayList<>();
        array.add(1);
        array.add(2);
        assertEquals("CollectionTest.testEquals", true, list.equals(array));
    }

    private static void testHashCode() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        assertEquals("CollectionTest.testHashCode", 30817, list.hashCode());
    }
}
