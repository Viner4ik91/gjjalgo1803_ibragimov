package com.getjavajob.training.algo1803.ibragimovv.lesson10;

import java.util.Arrays;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class InsertionSortTest {
    public static void main(String[] args) {
        testSort();
    }

    private static void testSort() {
        int[] initArray = new int[]{3, 7, 4, 9, 5, 2, 6, 1};
        int[] expected = new int[]{1, 2, 3, 4, 5, 6, 7, 9};
        InsertionSort is = new InsertionSort();
        is.sort(initArray);
        assertEquals("InsertionSortTest.testSort()", Arrays.toString(expected), Arrays.toString(initArray));
    }
}
