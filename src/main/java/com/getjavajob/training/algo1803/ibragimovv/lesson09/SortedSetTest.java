package com.getjavajob.training.algo1803.ibragimovv.lesson09;

import java.util.SortedSet;
import java.util.TreeSet;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class SortedSetTest {
    public static void main(String[] args) {
        testSubSet();
        testHeadSet();
        testTailSet();
        testFirst();
        testLast();
    }

    private static SortedSet<Integer> addSet() {
        SortedSet<Integer> set = new TreeSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        set.add(6);
        return set;
    }

    private static void testSubSet() {
        SortedSet<Integer> set = addSet();
        assertEquals("SortedSetTest.testSubSet", "[2, 3, 4, 5]", set.subSet(2, 6).toString());
    }

    private static void testHeadSet() {
        SortedSet<Integer> set = addSet();
        assertEquals("SortedSetTest.testHeadSet", "[1, 2, 3, 4]", set.headSet(5).toString());
    }

    private static void testTailSet() {
        SortedSet<Integer> set = addSet();
        assertEquals("SortedSetTest.testTailSet", "[3, 4, 5, 6]", set.tailSet(3).toString());
    }

    private static void testFirst() {
        SortedSet<Integer> set = addSet();
        assertEquals("SortedSetTest.testFirst", 1, set.first());
    }

    private static void testLast() {
        SortedSet<Integer> set = addSet();
        assertEquals("SortedSetTest.testLast", 6, set.last());
    }
}
