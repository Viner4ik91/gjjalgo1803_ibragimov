package com.getjavajob.training.algo1803.ibragimovv.lesson05;

/**
 * @author vinerI
 */
public interface Stack<E> {
    void push(E e); // add element to the top
    E pop(); // removes element from the top
}
