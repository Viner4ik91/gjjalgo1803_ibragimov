package com.getjavajob.training.algo1803.ibragimovv.lesson05;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.getjavajob.training.algo1803.ibragimovv.lesson05.CollectionUtils.filter;
import static com.getjavajob.training.algo1803.ibragimovv.lesson05.CollectionUtils.forAllDo;
import static com.getjavajob.training.algo1803.ibragimovv.lesson05.CollectionUtils.transform;
import static com.getjavajob.training.algo1803.ibragimovv.lesson05.CollectionUtils.transformMod;
import static com.getjavajob.training.util.Assert.assertEquals;
import static com.getjavajob.training.util.Assert.fail;

/**
 * @author vinerI
 */
public class CollectionUtilsTest {
    public static void main(String[] args) {
        testFilter();
        testTransform();
        testTransformMod();
        testForAllDo();
        testUnmodifiableCollection();
    }

    private static List<Employee> initEmployeeList() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Ivanov", "Andrey", 15000));
        employees.add(new Employee("Petrov", "Aleksei", 100000));
        employees.add(new Employee("Ivanova", "Luba", 30000));
        employees.add(new Employee("Sidorov", "Dima", 20000));
        return employees;
    }

    private static void testFilter() {
        List<Employee> employees = initEmployeeList();
        Predicate<Employee> predicate = new Predicate<Employee>() {
            @Override
            public boolean checkElement(Employee element) {
                return element.getLastName().toLowerCase().contains("ivanov");
            }
        };
        filter(employees, predicate);
        List<Employee> expected = new ArrayList<>();
        expected.add(new Employee("Ivanov", "Andrey", 15000));
        expected.add(new Employee("Ivanova", "Luba", 30000));
        assertEquals("CollectionUtilsTest.testFilter", expected, employees);
    }

    private static void testTransform() {
        List<Employee> employees = initEmployeeList();
        Transformer<Employee, String> transformer = new Transformer<Employee, String>() {
            @Override
            public String transformObject(Employee input) {
                return input.getLastName();
            }
        };
        List<String> expected = new ArrayList<>();
        expected.add("Ivanov");
        expected.add("Petrov");
        expected.add("Ivanova");
        expected.add("Sidorov");
        assertEquals("CollectionUtilsTest.testTransform", expected, transform(employees, transformer));
    }

    private static void testTransformMod() {
        List<Employee> employees = initEmployeeList();
        Transformer<Employee, String> transformer = new Transformer<Employee, String>() {
            @Override
            public String transformObject(Employee input) {
                return input.getLastName();
            }
        };
        List<String> expected = new ArrayList<>();
        expected.add("Ivanov");
        expected.add("Petrov");
        expected.add("Ivanova");
        expected.add("Sidorov");
        transformMod(employees, transformer);
        assertEquals("CollectionUtilsTest.testTransformMod", expected, employees);
    }

    private static void testForAllDo() {
        List<Employee> employees = initEmployeeList();

        CollectionLogic<Employee> collectionLogic = new CollectionLogic<Employee>() {
            @Override
            public Employee executionLogic(Employee element) {
                int salary = element.getSalary();
                element.setSalary(salary * 2);
                return element;
            }
        };
        List<Employee> expected = new ArrayList<>();
        expected.add(new Employee("Ivanov", "Andrey", 30000));
        expected.add(new Employee("Petrov", "Aleksei", 200000));
        expected.add(new Employee("Ivanova", "Luba", 60000));
        expected.add(new Employee("Sidorov", "Dima", 40000));
        assertEquals("CollectionUtilsTest.testForAllDo", expected, forAllDo(employees, collectionLogic));
    }

    private static void testUnmodifiableCollection() {
        List<Employee> employees = initEmployeeList();

        CollectionUtils.UnmodifiableCollection collection = new CollectionUtils.UnmodifiableCollection(employees);
        try {
            collection.remove(1);
            fail("UnsupportedOperationException");
        } catch (Exception e) {
            assertEquals("CollectionUtilsTest.testUnmodifiableCollection",
                    "UnsupportedOperationException", e.getClass().getSimpleName());
        }
    }


    private static class Employee {
        private String lastName;
        private String name;
        private int salary;

        private Employee(String lastName, String name, int salary) {
            this.lastName = lastName;
            this.name = name;
            this.salary = salary;
        }

        private String getLastName() {
            return lastName;
        }

        private void setLastName(String lastName) {
            this.lastName = lastName;
        }

        private String getName() {
            return name;
        }

        private void setName(String name) {
            this.name = name;
        }

        private int getSalary() {
            return salary;
        }

        private void setSalary(int salary) {
            this.salary = salary;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof Employee)) {
                return false;
            }
            Employee employee = (Employee) o;
            return getSalary() == employee.getSalary() && Objects.equals(getLastName(),
                    employee.getLastName()) && Objects.equals(getName(), employee.getName());
        }

        @Override
        public int hashCode() {

            return Objects.hash(getLastName(), getName(), getSalary());
        }
    }
}
