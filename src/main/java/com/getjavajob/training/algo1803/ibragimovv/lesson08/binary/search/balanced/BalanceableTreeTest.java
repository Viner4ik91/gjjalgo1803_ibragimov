package com.getjavajob.training.algo1803.ibragimovv.lesson08.binary.search.balanced;

import com.getjavajob.training.algo1803.ibragimovv.lesson07.Node;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class BalanceableTreeTest {
    public static void main(String[] args) {
        testRightRotation();
        testLeftRotation();
        testReduceSubtreeHeight();
    }

    private static void testRightRotation() {
        BalanceableTreeForTest<Integer> tree = new BalanceableTreeForTest<>();
        tree.addRoot(10);
        Node<Integer> rotationNode = tree.add(tree.root(), 5);
        tree.add(tree.root(), 15);
        tree.add(tree.root(), 3);
        tree.add(tree.root(), 6); //(10(5(3, 6), 15))
        tree.rotate(rotationNode);
        assertEquals("BalanceableTreeTest.testRightRotation", "(5(3), 10(6, 15))", tree.toStringImpl());
    }

    private static void testLeftRotation() {
        BalanceableTreeForTest<Integer> tree = new BalanceableTreeForTest<>();
        tree.addRoot(5);
        Node<Integer> pivot = tree.add(tree.root(), 10);
        tree.add(tree.root(), 15);
        tree.add(tree.root(), 3);
        tree.add(tree.root(), 6); //(5(3), 10(6, 15))
        tree.rotate(pivot);
        assertEquals("BalanceableTreeTest.testLeftRotation", "(10(5(3, 6), 15))", tree.toStringImpl());
    }

    private static void testReduceSubtreeHeight() {
        BalanceableTreeForTest<Integer> tree = new BalanceableTreeForTest<>();
        tree.addRoot(10);
        tree.add(tree.root(), 15);
        tree.add(tree.root(), 5);
        tree.add(tree.root(), 3);
        Node<Integer> pivot = tree.add(tree.root(), 8);
        tree.add(tree.root(), 6);
        tree.add(tree.root(), 9); //(10(5(3, 8(6, 9)), 15))
        tree.reduceSubtreeHeight(pivot);
        assertEquals("BalanceableTreeTest.testReduceSubtreeHeight", "(8(5(3, 6), 10(9, 15)))", tree.toStringImpl());
    }
}
