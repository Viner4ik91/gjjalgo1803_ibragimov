package com.getjavajob.training.algo1803.ibragimovv.lesson06;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class MapTest {
    public static void main(String[] args) {
        testSize();
        testIsEmpty();
        testContainsValue();
        testContainsKey();
        testGet();
        testPut();
        testRemove();
        testPutAll();
        testClear();
        testKeySet();
        testValues();
        testEntrySet();
    }

    private static Map<Integer, String> createHashMap() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "One");
        map.put(2, "Two");
        return map;
    }

    private static void testSize() {
        Map<Integer, String> map = createHashMap();
        assertEquals("MapTest.testSize", 2, map.size());
    }

    private static void testIsEmpty() {
        @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
        Map<Integer, Integer> emptyMap = new HashMap<>();
        assertEquals("MapTest.testIsEmpty", true, emptyMap.isEmpty());
    }

    private static void testContainsValue() {
        Map<Integer, String> map = createHashMap();
        assertEquals("MapTest.testContainsValue", true, map.containsValue("Two"));
    }

    private static void testContainsKey() {
        Map<Integer, String> map = createHashMap();
        assertEquals("MapTest.testContainsKey", true, map.containsKey(1));
    }

    private static void testGet() {
        Map<Integer, String> map = createHashMap();
        assertEquals("MapTest.testGet", "One", map.get(1));
    }

    private static void testPut() {
        Map<Integer, String> map = createHashMap();
        assertEquals("MapTest.testPut", "{1=One, 2=Two}", map.toString());
    }

    private static void testRemove() {
        Map<Integer, String> map = createHashMap();
        map.remove(1);
        assertEquals("MapTest.testRemove", "{2=Two}", map.toString());
    }

    private static void testPutAll() {
        Map<Integer, String> map1 = createHashMap();
        Map<Integer, String> map2 = new HashMap<>();
        map2.putAll(map1);
        assertEquals("MapTest.testPutAll", "{1=One, 2=Two}", map2.toString());
    }

    private static void testClear() {
        Map<Integer, String> map = createHashMap();
        map.clear();
        assertEquals("MapTest.testClear", 0, map.size());
    }

    private static void testKeySet() {
        Map<Integer, String> map = createHashMap();
        Set keySet = map.keySet();
        assertEquals("MapTest.testKeySet", "[1, 2]", keySet.toString());
    }

    private static void testValues() {
        Map<Integer, String> map = createHashMap();
        Collection values = map.values();
        assertEquals("MapTest.testValues", "[One, Two]", values.toString());
    }

    private static void testEntrySet() {
        Map<Integer, String> map = createHashMap();
        Set set = map.entrySet();
        assertEquals("MapTest.testEntrySet", "[1=One, 2=Two]", set.toString());
    }
}
