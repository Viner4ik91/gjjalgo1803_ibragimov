package com.getjavajob.training.algo1803.ibragimovv.lesson07.binary;

import com.getjavajob.training.algo1803.ibragimovv.lesson07.Node;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class LinkedBinaryTreeTest {
    public static void main(String[] args) {
        testAddRoot();
        testAdd();
        testAddLeft();
        testAddRight();
        testSet();
        testRemove();
        testLeft();
        testRight();
        testRoot();
        testParent();
        testSize();
        testSibling();
        testInOrder();
        testPreOrder();
        testPostOrder();
        testBreadthFirst();
    }

    private static void testAddRoot() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        assertEquals("LinkedBinaryTreeTest.testAddRoot", 10, tree.addRoot(10).getElement());
    }

    private static void testAdd() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> left = tree.add(root, 2);
        assertEquals("LinkedBinaryTreeTest.testAddFirst", left, tree.left(root));
        Node<Integer> right = tree.add(root, 3);
        assertEquals("LinkedBinaryTreeTest.testAddSecond", right, tree.right(root));
    }

    private static void testAddLeft() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        tree.addLeft(root, 10);
        assertEquals("LinkedBinaryTreeTest.testAddLeft", 10, tree.left(root).getElement());
    }

    private static void testAddRight() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        tree.addRight(root, 20);
        assertEquals("LinkedBinaryTreeTest.testAddRight", 20, tree.right(root).getElement());
    }

    private static void testSet() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> left = tree.addLeft(root, 20);
        tree.addRight(root, 30);
        tree.set(left, 40);
        assertEquals("LinkedBinaryTreeTest.testSet", 40, tree.left(root).getElement());
    }

    private static void testRemove() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> left = tree.addRight(root, 20);
        Node<Integer> right = tree.addLeft(root, 30);
        Node<Integer> leftOfLeft = tree.addLeft(left, 30);
        Node<Integer> rightOfRight = tree.addRight(right, 25);
        tree.remove(rightOfRight);
        tree.remove(leftOfLeft);
        assertEquals("LinkedBinaryTreeTest.testRemove", null, tree.right(right).getElement());
        assertEquals("LinkedBinaryTreeTest.testRemove", null, tree.left(left).getElement());
    }

    private static void testLeft() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> left = tree.addLeft(root, 20);
        assertEquals("LinkedBinaryTreeTest.testLeft", left, tree.left(root));
    }

    private static void testRight() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> right = tree.addRight(root, 20);
        assertEquals("LinkedBinaryTreeTest.testRight", right, tree.right(root));
    }

    private static void testRoot() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> left = tree.addLeft(root, 20);
        Node<Integer> right = tree.addRight(root, 30);
        Node<Integer> rightOfLeft = tree.addRight(left, 40);
        Node<Integer> rightOfRight = tree.addRight(right, 50);
        assertEquals("LinkedBinaryTreeTest.testRoot", root, tree.root());
    }

    private static void testParent() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> left = tree.addLeft(root, 20);
        Node<Integer> right = tree.addRight(root, 30);
        Node<Integer> rightOfLeft = tree.addRight(left, 40);
        Node<Integer> rightOfRight = tree.addRight(right, 50);
        assertEquals("LinkedBinaryTreeTest.testParent", right, tree.parent(rightOfRight));
    }

    private static void testSize() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> left = tree.addLeft(root, 20);
        Node<Integer> right = tree.addRight(root, 30);
        Node<Integer> rightOfLeft = tree.addRight(left, 40);
        Node<Integer> rightOfRight = tree.addRight(right, 50);
        tree.remove(rightOfRight);
        tree.add(rightOfLeft, 100);
        assertEquals("LinkedBinaryTreeTest.testSize", 5, tree.size());
    }

    private static void testSibling() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        Node<Integer> left = tree.addLeft(root, 2);
        Node<Integer> right = tree.addRight(root, 3);
        assertEquals("LinkedBinaryTreeTest.testSibling", right, tree.sibling(left));
    }

    private static LinkedBinaryTree<Integer> treeForOrders() {
        LinkedBinaryTree<Integer> tree = new LinkedBinaryTree<>();
        Node<Integer> root = tree.addRoot(1);
        tree.addLeft(root, 2);
        tree.addRight(root, 3);
        tree.addLeft(tree.left(root), 4);
        tree.addRight(tree.left(root), 5);
        tree.addRight(tree.right(root), 6);
        tree.addRight(tree.right(tree.right(root)), 7);
        return tree;
    }

    private static void testInOrder() {
        LinkedBinaryTree<Integer> tree = treeForOrders();
        List<Integer> actual = new ArrayList<>();
        for (Node<Integer> node : tree.inOrder()) {
            actual.add(node.getElement());
        }
        List<Integer> expected = new ArrayList<>();
        expected.add(4);
        expected.add(2);
        expected.add(5);
        expected.add(1);
        expected.add(3);
        expected.add(6);
        expected.add(7);
        assertEquals("LinkedBinaryTreeTest.testInOrder", expected, actual);
    }

    private static void testPreOrder() {
        LinkedBinaryTree<Integer> tree = treeForOrders();
        List<Integer> actual = new ArrayList<>();
        for (Node<Integer> node : tree.preOrder()) {
            actual.add(node.getElement());
        }
        List<Integer> expected = new ArrayList<>();
        expected.add(1);
        expected.add(2);
        expected.add(4);
        expected.add(5);
        expected.add(3);
        expected.add(6);
        expected.add(7);
        assertEquals("LinkedBinaryTreeTest.testPreOrder", expected, actual);
    }

    private static void testPostOrder() {
        LinkedBinaryTree<Integer> tree = treeForOrders();
        List<Integer> actual = new ArrayList<>();
        for (Node<Integer> node : tree.postOrder()) {
            actual.add(node.getElement());
        }
        List<Integer> expected = new ArrayList<>();
        expected.add(4);
        expected.add(5);
        expected.add(2);
        expected.add(7);
        expected.add(6);
        expected.add(3);
        expected.add(1);
        assertEquals("LinkedBinaryTreeTest.testPostOrder", expected, actual);
    }

    private static void testBreadthFirst() {
        LinkedBinaryTree<Integer> tree = treeForOrders();
        List<Integer> actual = new ArrayList<>();
        for (Node<Integer> node : tree.breadthFirst()) {
            actual.add(node.getElement());
        }
        List<Integer> expected = new ArrayList<>();
        expected.add(1);
        expected.add(2);
        expected.add(3);
        expected.add(4);
        expected.add(5);
        expected.add(6);
        expected.add(7);
        assertEquals("LinkedBinaryTreeTest.testPostOrder", expected, actual);
    }
}
