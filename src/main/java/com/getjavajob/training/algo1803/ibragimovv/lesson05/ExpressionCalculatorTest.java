package com.getjavajob.training.algo1803.ibragimovv.lesson05;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class ExpressionCalculatorTest {
    public static void main(String[] args) {
        testConversion();
        testCalculation();
    }

    private static void testConversion() {
        ExpressionCalculator conversion = new ExpressionCalculator();
        String input = "(((1 + 3) * (4 - 2)) / 2) * 5";
        String expected = "1 3 + 4 2 - * 2 / 5 *";
        assertEquals("ExpressionCalculatorTest.testConversion", expected, conversion.convertExpression(input));
    }

    private static void testCalculation() {
        ExpressionCalculator conversion = new ExpressionCalculator();
        String input = "1 3 + 4 2 - * 2 / 5 *";
        assertEquals("ExpressionCalculatorTest.testConversion", 20., conversion.calculateExpression(input));
    }
}
