package com.getjavajob.training.algo1803.ibragimovv.lesson06;

import java.util.LinkedHashSet;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class LinkedHashSetTest {
    public static void main(String[] args) {
        testAdd();
        testAddDuplicate();
        testOrder();
    }

    private static void testAdd() {
        LinkedHashSet<Integer> set = new LinkedHashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        assertEquals("LinkedHashSetTest.testAdd", "[1, 2, 3]", set.toString());
    }

    private static void testAddDuplicate() {
        LinkedHashSet<Integer> set = new LinkedHashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        assertEquals("LinkedHashSetTest.testAddDuplicate", false, set.add(1));
    }

    private static void testOrder() {
        LinkedHashSet<Integer> set = new LinkedHashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.remove(1);
        set.add(4);
        assertEquals("LinkedHashSetTest.testAdd", "[2, 3, 4]", set.toString());
    }
}
