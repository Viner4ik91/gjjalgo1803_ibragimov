package com.getjavajob.training.algo1803.ibragimovv.lesson06;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class AssociativeArrayTest {
    public static void main(String[] args) {
        testAdd();
        testAddOverlap();
        testReturnOldValueFromBucket();
        testAddNullKey();
        testRemove();
    }

    private static void testAdd() {
        AssociativeArray<Integer, String> array = new AssociativeArray<>();
        array.add(1, "first");
        array.add(1, "first");
        assertEquals("AssociativeArrayTest.testAdd", "first", array.get(1));
    }

    private static void testAddOverlap() {
        AssociativeArray<Integer, String> array = new AssociativeArray<>();
        array.add(1, "first");
        array.add(1, "second");
        assertEquals("AssociativeArrayTest.testAddOverlap", "second", array.get(1));
    }

    private static void testReturnOldValueFromBucket() {
        AssociativeArray<Integer, String> array = new AssociativeArray<>();
        array.add(1, "first");
        String oldValue = array.add(1, "second");
        assertEquals("AssociativeArrayTest.testReturnOldValueFromBucket", "first", oldValue);
    }

    private static void testAddNullKey() {
        AssociativeArray<Integer, String> array = new AssociativeArray<>();
        array.add(null, "first");
        assertEquals("AssociativeArrayTest.testAddNullKey", "first", array.get(null));
    }

    private static void testRemove() {
        AssociativeArray<Integer, String> array = new AssociativeArray<>();
        array.add(1, "first");
        array.remove(1);
        assertEquals("AssociativeArrayTest.testRemove", null, array.get(1));
    }
}
