package com.getjavajob.training.algo1803.ibragimovv.lesson09;

import java.util.NavigableMap;
import java.util.TreeMap;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class NavigableMapTest {
    public static void main(String[] args) {
        testLowerEntry();
        testLowerKey();
        testFloorEntry();
        testFloorKey();
        testCeilingEntry();
        testCeilingKey();
        testHigherEntry();
        testHigherKey();
        testFirstEntry();
        testLastEntry();
        testPollFirstEntry();
        testPollLastEntry();
        testDescendingMap();
        testNavigableKeySet();
        testDescendingKeySet();
    }

    private static NavigableMap<String, Integer> initMap() {
        NavigableMap<String, Integer> map = new TreeMap<>();
        map.put("A", 1);
        map.put("B", 2);
        map.put("C", 3);
        map.put("D", 4);
        map.put("E", 5);
        return map;
    }

    private static void testLowerEntry() {
        NavigableMap<String, Integer> map = initMap();
        assertEquals("NavigableMapTest.testLowerEntry", "C=3", map.lowerEntry("D").toString());
    }

    private static void testLowerKey() {
        NavigableMap<String, Integer> map = initMap();
        assertEquals("NavigableMapTest.testLowerKey", "C", map.lowerKey("D"));
    }

    private static void testFloorEntry() {
        NavigableMap<String, Integer> map = initMap();
        assertEquals("NavigableMapTest.testFloorEntry", "E=5", map.floorEntry("F").toString());
    }

    private static void testFloorKey() {
        NavigableMap<String, Integer> map = initMap();
        assertEquals("NavigableMapTest.testFloorKey", "E", map.floorKey("F"));
    }

    private static void testCeilingEntry() {
        NavigableMap<String, Integer> map = initMap();
        assertEquals("NavigableMapTest.testCeilingEntry", "A=1", map.ceilingEntry("A").toString());
    }

    private static void testCeilingKey() {
        NavigableMap<String, Integer> map = initMap();
        assertEquals("NavigableMapTest.testCeilingKey", "A", map.ceilingKey("A"));
    }

    private static void testHigherEntry() {
        NavigableMap<String, Integer> map = initMap();
        assertEquals("NavigableMapTest.testHigherEntry", "B=2", map.higherEntry("A").toString());
    }

    private static void testHigherKey() {
        NavigableMap<String, Integer> map = initMap();
        assertEquals("NavigableMapTest.testHigherKey", "B", map.higherKey("A"));
    }

    private static void testFirstEntry() {
        NavigableMap<String, Integer> map = initMap();
        assertEquals("NavigableMapTest.testFirstEntry", "A=1", map.firstEntry().toString());
    }

    private static void testLastEntry() {
        NavigableMap<String, Integer> map = initMap();
        assertEquals("NavigableMapTest.testLastEntry", "E=5", map.lastEntry().toString());
    }

    private static void testPollFirstEntry() {
        NavigableMap<String, Integer> map = initMap();
        map.pollFirstEntry();
        assertEquals("NavigableMapTest.testPollFirstEntry", "{B=2, C=3, D=4, E=5}", map.toString());
    }

    private static void testPollLastEntry() {
        NavigableMap<String, Integer> map = initMap();
        map.pollLastEntry();
        assertEquals("NavigableMapTest.testPollLastEntry", "{A=1, B=2, C=3, D=4}", map.toString());
    }

    private static void testDescendingMap() {
        NavigableMap<String, Integer> map = initMap();
        assertEquals("NavigableMapTest.testDescendingMap", "{E=5, D=4, C=3, B=2, A=1}", map.descendingMap().toString());
    }

    private static void testNavigableKeySet() {
        NavigableMap<String, Integer> map = initMap();
        assertEquals("NavigableMapTest.testNavigableKeySet", "[A, B, C, D, E]", map.navigableKeySet().toString());
    }

    private static void testDescendingKeySet() {
        NavigableMap<String, Integer> map = initMap();
        assertEquals("NavigableMapTest.testDescendingKeySet", "[E, D, C, B, A]", map.descendingKeySet().toString());
    }
}
