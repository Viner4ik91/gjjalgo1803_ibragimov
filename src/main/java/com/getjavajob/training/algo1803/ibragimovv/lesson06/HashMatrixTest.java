package com.getjavajob.training.algo1803.ibragimovv.lesson06;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class HashMatrixTest {
    public static void main(String[] args) {
        testStore();
    }

    private static void testStore() {
        HashMatrix<Integer> matrix = new HashMatrix<>();
        List<Integer> actual = new ArrayList<>();
        List<Integer> expected = new ArrayList<>();
        int n = 1000000;
        for (int i = 0; i < n; i++) {
            matrix.set(i, n, i);
            actual.add(matrix.get(i, n));
            expected.add(i);
        }
        assertEquals("HashMatrixTest.testStore", expected, actual);
    }
}
