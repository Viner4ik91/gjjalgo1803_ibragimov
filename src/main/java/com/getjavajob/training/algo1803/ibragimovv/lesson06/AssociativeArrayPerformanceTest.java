package com.getjavajob.training.algo1803.ibragimovv.lesson06;

import com.getjavajob.training.util.StopWatch;

import java.util.HashMap;
import java.util.Map;

/**
 * @author vinerI
 */
public class AssociativeArrayPerformanceTest {
    private static final int NUMBER_OF_ELEMENTS = 3_000_000;

    public static void main(String[] args) {
        AssociativeArray<Integer, Integer> array = new AssociativeArray<>();
        AssociativeArray<String, Integer> arrayKeys = new AssociativeArray<>();
        Map<Integer, Integer> map = new HashMap<>();
        Map<String, Integer> mapKeys = new HashMap<>();

        testAddAssociativeArray(array);
        testAddHashMap(map);
        testGetAssociativeArray(array);
        testGetHashMap(map);
        testRemoveAssociativeArray(array);
        testRemoveHashMap(map);
        testAddSpecialKeysAs(arrayKeys);
        testAddSpecialKeysHashMap(mapKeys);
    }

    private static void testAddAssociativeArray(AssociativeArray<Integer, Integer> array) {
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            array.add(i, i++);
        }
        System.out.println("--------- Addition ----------");
        System.out.println("AssociativeArray.add(key, value): " + time.getElapsedTime() + " ms");
    }

    private static void testAddHashMap(Map<Integer, Integer> map) {
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            map.put(i, i++);
        }
        System.out.println("HashMap.put(key, value): " + time.getElapsedTime() + " ms");
        System.out.println("-------- etc -------");
        System.out.println();
    }

    private static void testGetAssociativeArray(AssociativeArray<Integer, Integer> array) {

        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            array.get(i);
        }
        System.out.println("--------- Getting ----------");
        System.out.println("AssociativeArray.get(key, value): " + time.getElapsedTime() + " ms");
    }

    private static void testGetHashMap(Map<Integer, Integer> map) {

        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            map.get(i);
        }
        System.out.println("HashMap.get(key, value): " + time.getElapsedTime() + " ms");
        System.out.println("-------- etc -------");
        System.out.println();
    }

    private static void testRemoveAssociativeArray(AssociativeArray<Integer, Integer> array) {

        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            array.remove(i);
        }
        System.out.println("--------- Removing ----------");
        System.out.println("AssociativeArray.remove(key, value): " + time.getElapsedTime() + " ms");
    }

    private static void testRemoveHashMap(Map<Integer, Integer> map) {

        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS; i++) {
            map.remove(i);
        }
        System.out.println("HashMap.remove(key, value): " + time.getElapsedTime() + " ms");
        System.out.println("-------- etc -------");
        System.out.println();
    }

    private static void testAddSpecialKeysAs(AssociativeArray<String, Integer> array) {
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS / 2; i++) {
            array.add("polygenelubricants", i++);
            array.add("random", i++);
        }
        System.out.println("--------- AdditionKeys ----------");
        System.out.println("AssociativeArray.add(specialKey, value): " + time.getElapsedTime() + " ms");
    }

    private static void testAddSpecialKeysHashMap(Map<String, Integer> map) {
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_ELEMENTS / 2; i++) {
            map.put("polygenelubricants", i++);
            map.put("random", i++);
        }
        System.out.println("HashMap.put(specialKey, value): " + time.getElapsedTime() + " ms");
        System.out.println("-------- etc -------");
        System.out.println();
    }
}
