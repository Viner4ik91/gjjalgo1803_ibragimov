package com.getjavajob.training.algo1803.ibragimovv.lesson09;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeSet;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class NavigableSetTest {
    public static void main(String[] args) {
        testLower();
        testFloor();
        testCeiling();
        testHigher();
        testPollFirst();
        testPollLast();
        testIterator();
        testDescendingSet();
        testDescendingIterator();
    }

    private static NavigableSet<Integer> addSet() {
        NavigableSet<Integer> set = new TreeSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        set.add(6);
        return set;
    }

    private static void testLower() {
        NavigableSet<Integer> set = addSet();
        assertEquals("NavigableTest.testLower", 3, set.lower(4));
    }

    private static void testFloor() {
        NavigableSet<Integer> set = addSet();
        assertEquals("NavigableTest.testFloor", 4, set.floor(4));
    }

    private static void testCeiling() {
        NavigableSet<Integer> set = addSet();
        assertEquals("NavigableTest.testCeiling", 1, set.ceiling(0));
    }

    private static void testHigher() {
        NavigableSet<Integer> set = addSet();
        assertEquals("NavigableTest.testHigher", 5, set.higher(4));
    }

    private static void testPollFirst() {
        NavigableSet<Integer> set = addSet();
        assertEquals("NavigableTest.testPoolFirst", 1, set.pollFirst());
    }

    private static void testPollLast() {
        NavigableSet<Integer> set = addSet();
        assertEquals("NavigableTest.testPoolLast", 6, set.pollLast());
    }

    private static void testIterator() {
        NavigableSet<Integer> set = addSet();
        Iterator<Integer> it = set.iterator();
        StringBuilder sb = new StringBuilder();
        while (it.hasNext()) {
            sb.append(it.next());
        }
        assertEquals("NavigableTest.testIterator", "123456", sb.toString());
    }

    private static void testDescendingSet() {
        NavigableSet<Integer> set = addSet();
        assertEquals("NavigableTest.testDescendingSet", "[6, 5, 4, 3, 2, 1]", set.descendingSet().toString());
    }

    private static void testDescendingIterator() {
        NavigableSet<Integer> set = addSet();
        Iterator<Integer> it = set.descendingIterator();
        StringBuilder sb = new StringBuilder();
        while (it.hasNext()) {
            sb.append(it.next());
        }
        assertEquals("NavigableTest.testDescendingIterator", "654321", sb.toString());
    }
}
