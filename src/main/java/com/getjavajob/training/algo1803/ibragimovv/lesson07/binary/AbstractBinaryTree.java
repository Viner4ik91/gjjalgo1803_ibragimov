package com.getjavajob.training.algo1803.ibragimovv.lesson07.binary;

import com.getjavajob.training.algo1803.ibragimovv.lesson07.AbstractTree;
import com.getjavajob.training.algo1803.ibragimovv.lesson07.Node;

import java.util.Collection;

public abstract class AbstractBinaryTree<E> extends AbstractTree<E> implements BinaryTree<E> {
    @Override
    public Node<E> sibling(Node<E> n) throws IllegalArgumentException {
        if (parent(n) != null) {
            return left(parent(n)) == n ? right(parent(n)) : left(parent(n));
        } else {
            return null;
        }
    }

    @Override
    public Collection<Node<E>> children(Node<E> n) throws IllegalArgumentException {
        return null;
    }

    @Override
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {
        return 0;
    }

    /**
     *
     * @return an iterable collection of nodes of the tree in inorder
     */
    public Collection<Node<E>> inOrder() {
        return null;
    }
}
