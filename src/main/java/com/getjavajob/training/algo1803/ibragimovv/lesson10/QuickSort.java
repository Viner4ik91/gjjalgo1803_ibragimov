package com.getjavajob.training.algo1803.ibragimovv.lesson10;

/**
 * @author vinerI
 */
public class QuickSort {
    public void sort(int[] array) {
        quickSort(array, 0, array.length - 1);
    }

    public void sort(byte[] array) {
        quickSort(array, 0, array.length - 1);
    }

    private int partition(int[] array, int from, int to) {
        int pivot = array[from];
        int i = from;
        int j = to;

        while (i <= j) {
            while (array[i] < pivot) {
                i++;
            }
            while (array[j] > pivot) {
                j--;
            }
            if (i <= j) {
                int cache = array[i];
                array[i] = array[j];
                array[j] = cache;
                i++;
                j--;
            }
        }
        return i;
    }

    private int partition(byte[] array, int from, int to) {
        int pivot = array[from];
        int i = from;
        int j = to;

        while (i <= j) {
            while (array[i] < pivot) {
                i++;
            }
            while (array[j] > pivot) {
                j--;
            }
            if (i <= j) {
                byte cache = array[i];
                array[i] = array[j];
                array[j] = cache;
                i++;
                j--;
            }
        }
        return i;
    }

    private void quickSort(int[] array, int from, int to) {
        int index = partition(array, from, to);
        if (from < index - 1) {
            quickSort(array, from, index - 1);
        }
        if (index < to) {
            quickSort(array, index, to);
        }
    }

    private void quickSort(byte[] array, int from, int to) {
        int index = partition(array, from, to);
        if (from < index - 1) {
            quickSort(array, from, index - 1);
        }
        if (index < to) {
            quickSort(array, index, to);
        }
    }
}
