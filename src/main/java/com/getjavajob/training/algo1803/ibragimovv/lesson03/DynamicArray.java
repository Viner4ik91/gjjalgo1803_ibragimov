package com.getjavajob.training.algo1803.ibragimovv.lesson03;

import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

import static java.lang.System.arraycopy;

/**
 * @author vinerI
 */
public class DynamicArray {
    private Object[] elementData;
    private int size;
    private int modCount;

    @SuppressWarnings("unchecked")
    public DynamicArray(int specifiedSize) {
        if (specifiedSize > 0) {
            this.elementData = new Object[specifiedSize];
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " + specifiedSize);
        }
    }

    @SuppressWarnings("unchecked")
    public DynamicArray() {
        this.elementData = new Object[10];
    }

    private void checkIndexOutOfBounds(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException("ArrayIndexOutOfBoundsException");
        }
    }

    @SuppressWarnings("unchecked")
    private void checkSize() {
        if (size == elementData.length) {
            Object[] growArray = new Object[(int) (elementData.length * 1.5)];
            arraycopy(elementData, 0, growArray, 0, elementData.length);
            elementData = growArray;
        }
    }

    public boolean add(Object element) {
        checkSize();
        elementData[size] = element;
        size++;
        modCount++;
        return true;
    }

    public void add(int index, Object element) {
        checkIndexOutOfBounds(index);
        checkSize();
        if (index < size - 1) {
            arraycopy(elementData, index, elementData, index + 1, size - index);
        }
        elementData[index] = element;
        modCount++;
        size++;
    }

    public Object set(int index, Object element) {
        checkIndexOutOfBounds(index);
        Object oldValue = elementData[index];
        elementData[index] = element;
        return oldValue;
    }

    public Object get(int index) {
        checkIndexOutOfBounds(index);
        return elementData[index];
    }

    public Object remove(int index) {
        checkIndexOutOfBounds(index);
        modCount++;
        Object removedElement = elementData[index];
        if (index <= size - 1) {
            arraycopy(elementData, index + 1, elementData, index, size - index - 1);
        }
        if (index == size) {
            elementData[index] = null;
        }
        size--;
        return removedElement;
    }

    public boolean remove(Object element) {
        if (element == null) {
            for (int index = 0; index < size; index++)
                if (elementData[index] == null) {
                    remove(index);
                    return true;
                }
        } else {
            for (int index = 0; index < size; index++)
                if (element.equals(elementData[index])) {
                    remove(index);
                    return true;
                }
        }
        return false;
    }

    public int size() {
        return size;
    }

    public int indexOf(Object element) {
        if (element == null) {
            for (int i = 0; i < size; i++) {
                if (elementData[i] == null) {
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (element.equals(elementData[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    public boolean contains (Object element) {
        return indexOf(element) >= 0;
    }

    public Object[] toArray () {
        Object[] array = new Object[size];
        arraycopy(elementData, 0, array, 0, size);
        return array;
    }

    public ListIterator listIterator() {
        return new ListIterator();
    }

    public class ListIterator {
        int position = 0;
        int modCount;

        private ListIterator() {
            this.modCount = DynamicArray.this.modCount;
        }

        public boolean hasNext() {
         return position != size;
        }

        public Object next(){
            checkConcurrentModificationException();
            if (!hasNext()) {
                throw new NoSuchElementException("No elements");
            }
            return get(position++);
        }

        public boolean hasPrevious(){
            return position > 0;
        }

        public Object previous(){
            if (position <= 0) {
                throw new NoSuchElementException("No elements");
            }
            return get(--position);
        }

        public int nextIndex(){
            return position;
        }

        public int previousIndex(){
            return position - 1;
        }

        public void remove(){
            checkConcurrentModificationException();
            DynamicArray.this.remove(position);
            modCount = DynamicArray.this.modCount;
        }

        public void set(Object e){
            checkConcurrentModificationException();
            DynamicArray.this.set(position++, e);
        }

        public void add(Object e){
            checkConcurrentModificationException();
            DynamicArray.this.add(position++, e);
            modCount = DynamicArray.this.modCount;
        }

        private void checkConcurrentModificationException() {
            if (modCount != DynamicArray.this.modCount) {
                throw new ConcurrentModificationException("Error of modification the object");
            }
        }

    }
}