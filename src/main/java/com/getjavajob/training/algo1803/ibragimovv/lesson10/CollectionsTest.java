package com.getjavajob.training.algo1803.ibragimovv.lesson10;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import static com.getjavajob.training.util.Assert.assertEquals;
import static com.getjavajob.training.util.Assert.fail;
import static java.util.Arrays.asList;
import static java.util.Collections.addAll;
import static java.util.Collections.asLifoQueue;
import static java.util.Collections.binarySearch;
import static java.util.Collections.copy;
import static java.util.Collections.disjoint;
import static java.util.Collections.fill;
import static java.util.Collections.frequency;
import static java.util.Collections.indexOfSubList;
import static java.util.Collections.lastIndexOfSubList;
import static java.util.Collections.max;
import static java.util.Collections.min;
import static java.util.Collections.nCopies;
import static java.util.Collections.newSetFromMap;
import static java.util.Collections.replaceAll;
import static java.util.Collections.reverse;
import static java.util.Collections.rotate;
import static java.util.Collections.shuffle;
import static java.util.Collections.singleton;
import static java.util.Collections.sort;
import static java.util.Collections.swap;
import static java.util.Collections.unmodifiableCollection;

/**
 * @author vinerI
 */
public class CollectionsTest {
    public static void main(String[] args) {
        testSort();
        testSortComparator();
        testBinarySearch();
        testReverse();
        testShuffle();
        testSwap();
        testFill();
        testCopy();
        testMin();
        testMax();
        testRotate();
        testReplaceAll();
        testIndexOfSublist();
        testLastIndexOfSublist();
        testUnmodifiableCollection();
        testSingleton();
        testNCopies();
        testFrequency();
        testDisjoint();
        testAddAll();
        testNewSetFromMap();
        testAsLifoQueue();
    }

    private static void testSort() {
        Integer[] array = new Integer[]{3, 7, 4, 9, 5, 2, 6, 1};
        List<Integer> actual = asList(array);
        List<Integer> expected = asList(1, 2, 3, 4, 5, 6, 7, 9);
        sort(actual);
        assertEquals("Collections.testSort", expected, actual);
    }

    private static void testSortComparator() {
        Integer[] array = new Integer[]{3, 7, 4, 9, 5, 2, 6, 1};
        List<Integer> actual = asList(array);
        List<Integer> expected = asList(9, 7, 6, 5, 4, 3, 2, 1);
        Comparator c = Comparator.reverseOrder();
        sort(actual, c);
        assertEquals("CollectionsTest.testSortComparator", expected, actual);
    }

    private static void testBinarySearch() {
        List<Integer> list = asList(1, 2, 3, 4, 5, 6, 7, 8);
        assertEquals("CollectionsTest.testBinarySearch", 2, binarySearch(list, 3));
    }

    private static void testReverse() {
        List<Integer> list = asList(1, 2, 3, 4, 5, 6, 7, 8);
        List<Integer> expected = asList(8, 7, 6, 5 ,4 ,3 ,2, 1);
        reverse(list);
        assertEquals("CollectionsTest.testReverse", expected, list);
    }

    private static void testShuffle() {
        List<Integer> list = asList(1, 2, 3, 4, 5, 6, 7, 8);
        shuffle(list);
        List<Integer> firstList = asList(2, 3, 4, 5, 7, 8, 1);
        assertEquals("CollectionsTest.testShuffle", false, list.equals(firstList));
    }

    private static void testSwap() {
        List<Integer> list = asList(1, 2, 3, 4, 5, 6, 7, 8);
        List<Integer> expected = asList(1, 7, 3, 4, 5, 6, 2, 8);
        swap(list, 1, 6);
        assertEquals("CollectionsTest.testSwap", expected, list);
    }

    private static void testFill() {
        List<Integer> list = asList(1, 2, 3, 4, 5, 6, 7, 8);
        fill(list, 5);
        List<Integer> expected = asList(5, 5, 5, 5, 5, 5, 5, 5);
        assertEquals("CollectionsTest.testFill", expected, list);
    }

    private static void testCopy() {
        List<Integer> list = asList(1, 2, 3, 4, 5, 6, 7, 8);
        List<Integer> expected = asList(5, 5, 5, 5, 5, 5, 5, 5);
        copy(list, expected);
        assertEquals("CollectionsTest.testCopy", expected, list);
    }

    private static void testMin() {
        List<Integer> list = asList(1, 2, 3, 4, -1, 5, 6, 7, 8);
        assertEquals("CollectionsTest.testMin", -1, min(list));
    }

    private static void testMax() {
        List<Integer> list = asList(1, 2, 3, 4, -1, 5, 6, 7, 8);
        assertEquals("CollectionsTest.testMax", 8, max(list));
    }

    private static void testRotate() {
        List<Integer> list = asList(1, 2, 3, 4, 5, 6, 7, 8);
        rotate(list, 4);
        List<Integer> expected = asList(5, 6, 7, 8, 1, 2, 3, 4);
        assertEquals("CollectionsTest.testRotate", expected, list);
    }

    private static void testReplaceAll() {
        List<Integer> list = asList(1, 1, 1, 1, 5, 6, 7, 8);
        replaceAll(list, 1, 10);
        List<Integer> expected = asList(10, 10, 10, 10, 5, 6, 7, 8);
        assertEquals("CollectionsTest.testReplaceAll", expected, list);
    }

    private static void testIndexOfSublist() {
        List<Integer> list = asList(1, 2, 3, 4, 5, 6, 7, 8);
        List<Integer> sublist = asList(1, 2, 3, 4);
        assertEquals("CollectionsTest.testIndexOfSublist", 0, indexOfSubList(list, sublist));
    }

    private static void testLastIndexOfSublist() {
        List<Integer> list = asList(1, 2, 3, 4, 5, 1, 2, 3, 4, 5);
        List<Integer> sublist = asList(1, 2, 3, 4);
        assertEquals("CollectionsTest.testLastIndexOfSublist", 5, lastIndexOfSubList(list, sublist));
    }

    private static void testUnmodifiableCollection() {
        List<Integer> list = asList(1, 2, 3, 4, 5, 1, 2, 3, 4, 5);
        unmodifiableCollection(list);
        try {
            list.remove(4);
            fail("Test failed");
        } catch (Exception e) {
            assertEquals("UnsupportedOperationException", e.getClass().getSimpleName());
        }
    }

    private static void testSingleton() {
        Set<Integer> set = singleton(120);
        assertEquals("CollectionsTest.testSingleton", "[120]", set.toString());
    }

    private static void testNCopies() {
        Integer number = 5;
        List<Integer> list = new ArrayList<>(nCopies(10, number));
        assertEquals("CollectionsTest.testNCopies", "[5, 5, 5, 5, 5, 5, 5, 5, 5, 5]", list.toString());
    }

    private static void testFrequency() {
        List<Integer> list = asList(3, 1, 4, 2, 5, 3, 7, 8, 3, 3, 3);
        assertEquals("CollectionsTest.testFrequency", 5, frequency(list, 3));
    }

    private static void testDisjoint() {
        List<Integer> list = asList(3, 1, 4, 2, 5, 3, 7, 8, 3, 3, 3);
        List<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        assertEquals("CollectionsTest.testDisjoint", false, disjoint(list, arrayList));
    }

    private static void testAddAll() {
        List<Integer> list = new ArrayList<>(asList(1, 2, 3, 4));
        addAll(list, 5, 6, 7, 8, 9);
        List<Integer> expected = asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        assertEquals("CollectionsTest.testAddAll", expected, list);
    }

    private static void testNewSetFromMap() {
        Map<Integer, Boolean> map = new HashMap<>();
        Set<Integer> set = newSetFromMap(map);
        map.put(1, true);
        map.put(2, false);
        map.put(3, true);

        assertEquals("CollectionsTest.testNewSetFromMap", "[1, 2, 3]", set.toString());
    }

    private static void testAsLifoQueue() {
        Deque<Integer> dequeList = new ArrayDeque<>();
        dequeList.add(4);
        dequeList.addLast(3);
        dequeList.addLast(2);
        dequeList.addLast(1);
        Queue<Integer> list = asLifoQueue(dequeList);
        assertEquals("CollectionsTest.testAsLifoQueue", "[4, 3, 2, 1]", list.toString());
    }
}
