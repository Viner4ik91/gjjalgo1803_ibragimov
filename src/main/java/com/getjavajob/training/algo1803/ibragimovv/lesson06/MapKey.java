package com.getjavajob.training.algo1803.ibragimovv.lesson06;

import java.util.Objects;

/**
 * @author vinerI
 */
public class MapKey {
    private int i;
    private int j;

    MapKey(int i, int j) {
        this.i = i;
        this.j = j;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MapKey)) {
            return false;
        }
        MapKey mapKey = (MapKey) o;
        return i == mapKey.i && j == mapKey.j;
    }

    @Override
    public int hashCode() {

        return Objects.hash(i, j);
    }
}
