package com.getjavajob.training.algo1803.ibragimovv.lesson04;

import com.getjavajob.training.util.StopWatch;

import java.util.LinkedList;
import java.util.List;

/**
 * @author vinerI
 */
public class DoublyLinkedListPerformanceTest {
    private static final int NUMBER_OF_NODES = 100_000;

    public static void main(String[] args) {
        testAddToBeginDoublyLinkedList();
        testAddToBeginLinkedList();
        testAddToMiddleDoublyLinkedList();
        testAddToMiddleLinkedList();
        testAddToEndDoublyLinkedList();
        testAddToEndLinkedList();
        testRemoveFromBeginDoublyLinkedList();
        testRemoveFromBeginLinkedList();
        testRemoveFromMiddleDoublyLinkedList();
        testRemoveFromMiddleLinkedList();
        testRemoveFromEndDoublyLinkedList();
        testRemoveFromEndLinkedList();
    }

    private static void testAddToBeginDoublyLinkedList() {
        DoublyLinkedList<Integer> linkedList = new DoublyLinkedList<>();
        linkedList.add(0);
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_NODES * 60; i++) {
            linkedList.add(0, i);
        }
        System.out.println("-------- Addition to the beginning --------");
        System.out.println("DoublyLinkedList.add(e): " + time.getElapsedTime() + " ms");
    }

    private static void testAddToBeginLinkedList() {
        List<Integer> list = new LinkedList<>();
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_NODES * 60; i++) {
            list.add(0, i);
        }
        System.out.println("LinkedList.add(e): " + time.getElapsedTime() + " ms");
        System.out.println("-------- etc -------");
        System.out.println();
    }

    private static void testAddToMiddleDoublyLinkedList() {
        DoublyLinkedList<Integer> linkedList = new DoublyLinkedList<>();
        linkedList.add(0);
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_NODES / 3; i++) {
            linkedList.add(linkedList.size() / 2, i);
        }
        System.out.println("-------- Addition to the middle --------");
        System.out.println("DoublyLinkedList.add(e): " + time.getElapsedTime() + " ms");
    }

    private static void testAddToMiddleLinkedList() {
        List<Integer> list = new LinkedList<>();
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 0; i < NUMBER_OF_NODES / 3; i++) {
            list.add(list.size() / 2, i);
        }
        System.out.println("LinkedList.add(e): " + time.getElapsedTime() + " ms");
        System.out.println("-------- etc -------");
        System.out.println();
    }

    private static void testAddToEndDoublyLinkedList() {
        DoublyLinkedList<Integer> linkedList = new DoublyLinkedList<>();
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 1; i < NUMBER_OF_NODES * 61; i++) {
            linkedList.add(i);
        }
        System.out.println("-------- Addition to the end --------");
        System.out.println("DoublyLinkedList.add(e): " + time.getElapsedTime() + " ms");
    }

    private static void testAddToEndLinkedList() {
        List<Integer> list = new LinkedList<>();
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 1; i < NUMBER_OF_NODES * 61; i++) {
            list.add(i);
        }
        System.out.println("LinkedList.add(e): " + time.getElapsedTime() + " ms");
        System.out.println("-------- etc -------");
        System.out.println();
    }

    private static void testRemoveFromBeginDoublyLinkedList() {
        DoublyLinkedList<Integer> linkedList = new DoublyLinkedList<>();
        for (int i = 0; i < NUMBER_OF_NODES * 60; i++) {
            linkedList.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 1; i < NUMBER_OF_NODES * 60; i++) {
            linkedList.remove(0);
        }
        System.out.println("-------- Remove from the beginning --------");
        System.out.println("DoublyLinkedList.add(e): " + time.getElapsedTime() + " ms");
    }

    private static void testRemoveFromBeginLinkedList() {
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < NUMBER_OF_NODES * 60; i++) {
            list.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 1; i < NUMBER_OF_NODES * 60; i++) {
            list.remove(0);
        }
        System.out.println("LinkedList.add(e): " + time.getElapsedTime() + " ms");
        System.out.println("-------- etc -------");
        System.out.println();
    }

    private static void testRemoveFromMiddleDoublyLinkedList() {
        DoublyLinkedList<Integer> linkedList = new DoublyLinkedList<>();
        for (int i = 0; i < NUMBER_OF_NODES / 2; i++) {
            linkedList.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 1; i < NUMBER_OF_NODES / 2; i++) {
            linkedList.remove(linkedList.size() / 2);
        }
        System.out.println("-------- Remove from the middle --------");
        System.out.println("DoublyLinkedList.add(e): " + time.getElapsedTime() + " ms");
    }

    private static void testRemoveFromMiddleLinkedList() {
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < NUMBER_OF_NODES / 2; i++) {
            list.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 1; i < NUMBER_OF_NODES / 2; i++) {
            list.remove(list.size() / 2);
        }
        System.out.println("LinkedList.add(e): " + time.getElapsedTime() + " ms");
        System.out.println("-------- etc -------");
        System.out.println();
    }

    private static void testRemoveFromEndDoublyLinkedList() {
        DoublyLinkedList<Integer> linkedList = new DoublyLinkedList<>();
        for (int i = 1; i < NUMBER_OF_NODES * 60; i++) {
            linkedList.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 1; i < NUMBER_OF_NODES * 60; i++) {
            linkedList.remove(linkedList.size() - 1);
        }
        System.out.println("-------- Remove from the end --------");
        System.out.println("DoublyLinkedList.add(e): " + time.getElapsedTime() + " ms");
    }

    private static void testRemoveFromEndLinkedList() {
        List<Integer> list = new LinkedList<>();
        for (int i = 0; i < NUMBER_OF_NODES * 60; i++) {
            list.add(i);
        }
        StopWatch time = new StopWatch();
        time.start();
        for (int i = 1; i < NUMBER_OF_NODES * 60; i++) {
            list.remove(list.size() - 1);
        }
        System.out.println("LinkedList.add(e): " + time.getElapsedTime() + " ms");
        System.out.println("-------- etc -------");
        System.out.println();
    }
}