package com.getjavajob.training.algo1803.ibragimovv.lesson01;

/**
 * @author vinerI
 */
public class Task07 {
    public static void main(String[] args) {
        System.out.println(solveBitwiseSwapOne(2, 3));
        System.out.println(solveBitwiseSwapTwo(2, 3));
        System.out.println(solveArithmeticSwapOne(2, 3));
        System.out.println(solveArithmeticSwapTwo(2, 3));
    }

    public static String solveBitwiseSwapOne(int firstValue, int secondValue){
        firstValue = firstValue ^ secondValue;
        secondValue = firstValue ^ secondValue;
        firstValue = firstValue ^ secondValue;
        return firstValue + " " + secondValue;
    }

    public static String solveBitwiseSwapTwo(int firstValue, int secondValue) {
        firstValue = ~firstValue & secondValue | ~secondValue & firstValue;
        secondValue = ~firstValue & secondValue | ~secondValue & firstValue;
        firstValue = ~firstValue & secondValue | ~secondValue & firstValue;
        return firstValue + " " + secondValue;
    }

    public static String solveArithmeticSwapOne(int firstValue, int secondValue) {
        firstValue = firstValue + secondValue;
        secondValue = firstValue - secondValue;
        firstValue = firstValue - secondValue;
        return firstValue + " " + secondValue;
    }

    public static String solveArithmeticSwapTwo(int firstValue, int secondValue) {
        firstValue = firstValue * secondValue;
        secondValue = firstValue / secondValue;
        firstValue = firstValue / secondValue;
        return firstValue + " " + secondValue;
    }
}
