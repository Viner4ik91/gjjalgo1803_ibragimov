package com.getjavajob.training.algo1803.ibragimovv.lesson07.binary;

import com.getjavajob.training.algo1803.ibragimovv.lesson07.Node;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> {

    // nonpublic utility

    protected Node<E> root;
    private int size;

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     *
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n instanceof NodeImpl) {
            return (NodeImpl<E>) n;
        } else {
            throw new IllegalArgumentException();
        }
    }

    // update methods supported by this class

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        if (root != null) {
            throw new IllegalStateException("Root is already defined");
        }
        root = createNode(e);
        size++;
        return root;
    }

    protected NodeImpl<E> createNode(E e) {
        return new NodeImpl<>(e);
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        if (left(n) == null) {
            return addLeft(n, e);
        } else if (right(n) == null) {
            return addRight(n, e);
        } else {
            throw new IllegalArgumentException("Childes are defined");
        }
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> parentNode = validate(n);
        if (parentNode.getLeftChild() == null) {
            NodeImpl<E> leftNode = createNode(e);
            parentNode.setLeftChild(leftNode);
            leftNode.setParent(parentNode);
            size++;
            return leftNode;
        } else {
            throw new IllegalArgumentException("Left child is defined");
        }
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> parentNode = validate(n);
        if (parentNode.getRightChild() == null) {
            NodeImpl<E> rightNode = createNode(e);
            parentNode.setRightChild(rightNode);
            rightNode.setParent(parentNode);
            size++;
            return rightNode;
        } else {
            throw new IllegalArgumentException("Right child is defined");
        }
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        NodeImpl<E> node = validate(n);
        E replacedElement = node.getElement();
        node.setElement(e);
        return replacedElement;
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @return replace element
     * @throws IllegalArgumentException
     */
    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        if (n != null) {
            NodeImpl<E> node = validate(n);
            if (node.getLeftChild() != null || node.getRightChild() != null) {
                throw new IllegalArgumentException("Node has a child");
            }
            if (left(parent(node)) != null) {
                set(left(parent(node)), null);
            } else if (right(parent(node)) != null) {
                set(right(parent(node)), null);
            }
            size--;
            return node.getElement();
        } else {
            throw new IllegalArgumentException();
        }
    }

    // {@link Tree} and {@link BinaryTree} implementations

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        return validate(p).getLeftChild();
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        return validate(p).getRightChild();
    }

    @Override
    public Node<E> root() {
        return root;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        return validate(n).getParent();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        LinkedList<E> list = new LinkedList<>();
        for (Node<E> n : nodes()) {
            list.add(n.getElement());
        }
        return list.iterator();
    }

    @Override
    public Collection<Node<E>> nodes() {
        return inOrder();
    }

    @Override
    public Collection<Node<E>> inOrder() {
        if (root == null) {
            return null;
        }
        Stack<NodeImpl<E>> s = new Stack<>();
        List<Node<E>> list = new ArrayList<>();
        NodeImpl<E> currentNode = validate(root);
        while (!s.empty() || currentNode != null) {
            if (currentNode != null) {
                s.push(currentNode);
                currentNode = currentNode.leftChild;
            } else {
                NodeImpl<E> n = s.pop();
                list.add(n);
                currentNode = n.rightChild;
            }
        }
        return list;
    }

    @Override
    public Collection<Node<E>> preOrder() {
        if (root == null) {
            return null;
        }
        Stack<NodeImpl<E>> s = new Stack<>();
        List<Node<E>> list = new ArrayList<>();
        s.push(validate(root));
        while (!s.empty()) {
            NodeImpl<E> node = s.pop();
            list.add(node);
            if (node.rightChild != null) {
                s.push(node.rightChild);
            }
            if (node.leftChild != null) {
                s.push(node.leftChild);
            }
        }
        return list;
    }

    @Override
    public Collection<Node<E>> postOrder() {
        if (root == null) {
            return null;
        }
        Stack<NodeImpl<E>> s = new Stack<>();
        List<Node<E>> list = new ArrayList<>();
        s.push(validate(root));
        while (!s.empty()) {
            NodeImpl<E> node = s.peek();
            if (node.leftChild == null && node.rightChild == null) {
                NodeImpl<E> popNode = s.pop();
                list.add(popNode);
            } else {
                if (node.rightChild != null) {
                    s.push(node.rightChild);
                    node.rightChild = null;
                }
                if (node.leftChild != null) {
                    s.push(node.leftChild);
                    node.leftChild = null;
                }
            }
        }
        return list;
    }

    @Override
    public Collection<Node<E>> breadthFirst() {
        List<Node<E>> list = new ArrayList<>();
        if (root == null) {
            return list;
        } else {
            Queue<Node<E>> q = new LinkedList<>();
            q.add(root);
            while (q.peek() != null) {
                NodeImpl<E> temp = validate(q.remove());
                list.add(temp);
                if (temp.leftChild != null) {
                    q.add(temp.leftChild);
                }
                if (temp.rightChild != null) {
                    q.add(temp.rightChild);
                }
            }
        }
        return list;
    }

    protected static class NodeImpl<E> implements Node<E> {
        protected E val;
        private NodeImpl<E> parent;
        private NodeImpl<E> leftChild;
        private NodeImpl<E> rightChild;

        public NodeImpl(E val) {
            this.val = val;
        }

        public NodeImpl<E> getParent() {
            return parent;
        }

        public void setParent(NodeImpl<E> parent) {
            this.parent = parent;
        }

        public NodeImpl<E> getLeftChild() {
            return leftChild;
        }

        public void setLeftChild(NodeImpl<E> leftChild) {
            this.leftChild = leftChild;
        }

        public NodeImpl<E> getRightChild() {
            return rightChild;
        }

        public void setRightChild(NodeImpl<E> rightChild) {
            this.rightChild = rightChild;
        }

        @Override
        public E getElement() {
            return val;
        }

        public void setElement(E e) {
            this.val = e;
        }
    }
}
