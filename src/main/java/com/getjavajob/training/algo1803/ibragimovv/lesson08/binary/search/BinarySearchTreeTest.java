package com.getjavajob.training.algo1803.ibragimovv.lesson08.binary.search;

import com.getjavajob.training.algo1803.ibragimovv.lesson07.Node;

import java.util.Comparator;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class BinarySearchTreeTest {
    public static void main(String[] args) {
        testCompare();
        testTreeSearch();
        testRemove();
        testToString();
    }

    private static void testCompare() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>(Comparator.comparingInt(value -> value));
        assertEquals("BinarySearchTreeTest.testCompare", true, tree.compare(5, 1) > 0);
    }


    private static void testTreeSearch() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        tree.addRoot(10);
        tree.add(tree.root(), 9);
        Node<Integer> expected = tree.add(tree.root(), 15);
        tree.add(tree.root(), 8);
        tree.add(tree.root(), 11);
        tree.add(tree.root(), 17);
        assertEquals("BinarySearchTreeTest.testTreeSearch", expected, tree.treeSearch(expected, 15));
    }
    private static void testRemove() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        tree.addRoot(10);
        tree.add(tree.root(), 5);
        Node<Integer> deleted = tree.add(tree.root(), 15);
        tree.add(tree.root(), 7);
        tree.add(tree.root(), 4);
        tree.add(tree.root(), 17);
        tree.add(tree.root(), 11); //(10(5(4, 7), 15(11, 17)))
        tree.remove(deleted);
        assertEquals("BinarySearchTreeTest.testRemove", "(10(5(4, 7), 11(null, 17)))", tree.toStringImpl());
    }

    private static void testToString() {
        BinarySearchTree<Integer> tree = new BinarySearchTree<>();
        tree.addRoot(10);
        tree.add(tree.root(), 5);
        tree.add(tree.root(), 15);
        tree.add(tree.root(), 7);
        tree.add(tree.root(), 4);
        tree.add(tree.root(), 17);
        tree.add(tree.root(), 11);
        assertEquals("BinarySearchTreeTest.testToString", "(10(5(4, 7), 15(11, 17)))", tree.toStringImpl());
    }
}
