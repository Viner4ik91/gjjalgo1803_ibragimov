package com.getjavajob.training.algo1803.ibragimovv.lesson06;

import java.util.HashSet;
import java.util.Set;

import static com.getjavajob.training.util.Assert.assertEquals;

/**
 * @author vinerI
 */
public class SetTest {
    public static void main(String[] args) {
        testAddWithoutDuplicates();
        testAddDuplicates();
        testAddAll();
    }

    private static void testAddWithoutDuplicates() {
        Set<Integer> set = new HashSet<>();
        assertEquals("SetTest.testAddWithoutDuplicates", true, set.add(1));
    }

    private static void testAddDuplicates() {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        assertEquals("SetTest.testAddWithoutDuplicates", false, set.add(1));
    }

    private static void testAddAll() {
        Set<Integer> set = new HashSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
        Set<Integer> newSet = new HashSet<>();
        newSet.addAll(set);
        assertEquals("SetTest.testAddAll", set.toString(), newSet.toString());
    }
}
